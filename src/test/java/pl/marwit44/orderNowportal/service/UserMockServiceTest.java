package pl.marwit44.orderNowportal.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.exceptions.UserNotFoundException;
import pl.marwit44.orderNowportal.model.TokenTypes;
import pl.marwit44.orderNowportal.model.UserModel;
import pl.marwit44.orderNowportal.model.UserRole;
import pl.marwit44.orderNowportal.model.VerificationModel;
import pl.marwit44.orderNowportal.repository.UserRepository;
import pl.marwit44.orderNowportal.repository.VerificationRepository;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@RunWith(MockitoJUnitRunner.class)
public class UserMockServiceTest {

    private UserService userService;

    @Mock
    private VerificationRepository verificationRepository;

    @Mock
    private UserRepository userRepository;

    private MailingService mailingService;
    private MailTemplatesService mailTemplatesService;

    @Before
    public void setup() {
        userService = new UserService(userRepository, verificationRepository, mailingService, mailTemplatesService);

        VerificationModel token = VerificationModel.Builder.aVerificationModel()
                .withToken("token")
                .withType(TokenTypes.ACTIVATION)
                .withExpiryDate(LocalDateTime.now().plusMinutes(1))
                .build();

        UserModel user = UserModel.Builder.anUserModel()
                .withFirstName("anna")
                .withLastName("wal")
                .withEmail("annaWal@gmail.com")
                .withRegisterDate(LocalDateTime.now())
                .withLastLogin(LocalDateTime.now())
                .withRole(UserRole.ROLE_USER)
                .build();

        VerificationModel token2 = VerificationModel.Builder.aVerificationModel()
                .withToken("token2")
                .withExpiryDate(LocalDateTime.now().minusMinutes(1))
                .build();

        token.setUserModel(user);
        user.setVerificationModel(token);

        Mockito.when(verificationRepository.findByToken("token")).thenReturn(token);
        Mockito.when(verificationRepository.findByToken("token2")).thenReturn(token2);

    }

    @Test
    public void shouldCheckIsTokenIsCorrect() {

        //Given
        String token = "token";
        String token2 = "token2";

        //When
        boolean tokenCorrect = userService.isTokenCorrect(token, TokenTypes.ACTIVATION);
        boolean tokenIncorrect = userService.isTokenCorrect(token2, TokenTypes.ACTIVATION);
        boolean tokenNotExist = userService.isTokenCorrect("token_not_exist", TokenTypes.ACTIVATION);

        //Then
        Assertions.assertThat(tokenCorrect).isTrue();
        Assertions.assertThat(tokenIncorrect).isFalse();
        Assertions.assertThat(tokenNotExist).isFalse();
    }

    @Test
    public void shouldGetUserFromToken() throws UserNotFoundException {

        //Given
        String token = "token";

        //When
        User user = userService.getUserFromToken(token);

        //Then
        Assertions.assertThat(user.getFirstName().equals("anna")).isTrue();
        Assertions.assertThat(user.getLastName().equals("wal")).isTrue();
        Assertions.assertThat(user.getEmail().equals("annaWal@gmail.com")).isTrue();
    }

    @Test
    public void shouldNotGetUserFromToken() {

        //Given
        String token = "tokenAbcd";

        //When
        Throwable throwable = catchThrowable(() -> userService.getUserFromToken(token));

        //Then
        assertThat(throwable).isInstanceOf(UserNotFoundException.class);
    }
}