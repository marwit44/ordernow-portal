package pl.marwit44.orderNowportal.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowportal.model.UserModel;
import pl.marwit44.orderNowportal.model.UserRole;
import pl.marwit44.orderNowportal.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class AuthorizationServiceTest {

    private AuthorizationService authorizationService;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void init() {

        authorizationService = new AuthorizationService(userRepository);

        UserModel user = UserModel.Builder.anUserModel()
                .withFirstName("mar")
                .withLastName("wit")
                .withEmail("marWit@wp.pl")
                .withRegisterDate(LocalDateTime.now())
                .withLastLogin(LocalDateTime.now())
                .withRole(UserRole.ROLE_USER)
                .build();

        UserModel user2 = UserModel.Builder.anUserModel()
                .withFirstName("mark")
                .withLastName("zuck")
                .withEmail("markZuck@interia.pl")
                .withRegisterDate(LocalDateTime.now())
                .withLastLogin(LocalDateTime.now())
                .withRole(UserRole.ROLE_USER)
                .build();

        userRepository.save(user);
        userRepository.save(user2);
    }

    @Test
    public void shouldValidateTokenCorrect() throws TokenAuthorizationException {

        //Given
        List<UserModel> userList = userRepository.findAll();
        UserModel userForTest = userList.get(0);

        String token = authorizationService.generateToken(userForTest.getId(), userForTest.getLastLogin(), userForTest.getRole());

        //When
        User user = authorizationService.validateToken(token, userForTest.getRole());

        //Then
        Assertions.assertThat(user.getFirstName().equals(userForTest.getFirstName())).isTrue();
        Assertions.assertThat(user.getLastName().equals(userForTest.getLastName())).isTrue();
        Assertions.assertThat(user.getEmail().equals(userForTest.getEmail())).isTrue();
    }

    @Test
    public void shouldNotValidateTokenCorrect() {

        //Given
        List<UserModel> userList = userRepository.findAll();
        UserModel userForTest = userList.get(0);
        UserModel userForCheck = userList.get(1);

        String token = authorizationService.generateToken(userForTest.getId(), userForTest.getLastLogin(), userForTest.getRole());

        //When
        User user = authorizationService.validateToken(token, userForTest.getRole());

        //Then
        Assertions.assertThat(user.getFirstName().equals(userForCheck.getFirstName())).isFalse();
        Assertions.assertThat(user.getLastName().equals(userForCheck.getLastName())).isFalse();
        Assertions.assertThat(user.getEmail().equals(userForCheck.getEmail())).isFalse();
    }
}