package pl.marwit44.orderNowportal.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.marwit44.orderNowportal.dto.Address;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.exceptions.TokenNotExistException;
import pl.marwit44.orderNowportal.model.AddressModel;
import pl.marwit44.orderNowportal.model.TokenTypes;
import pl.marwit44.orderNowportal.model.UserModel;
import pl.marwit44.orderNowportal.model.UserRole;
import pl.marwit44.orderNowportal.model.VerificationModel;
import pl.marwit44.orderNowportal.repository.UserRepository;
import pl.marwit44.orderNowportal.repository.VerificationRepository;
import pl.marwit44.orderNowportal.transformer.AddressTransformer;
import pl.marwit44.orderNowportal.transformer.UserTransformer;
import pl.marwit44.orderNowportal.transformer.VerificationTransformer;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserServiceTest {

    private UserService userService;

    @Autowired
    private VerificationRepository verificationRepository;

    @Autowired
    private UserRepository userRepository;

    private MailingService mailingService;
    private MailTemplatesService mailTemplatesService;

    @Before
    public void setup() {
        userService = new UserService(userRepository, verificationRepository, mailingService, mailTemplatesService);

        UserModel user = UserModel.Builder.anUserModel()
                .withFirstName("mar")
                .withLastName("wit")
                .withEmail("marWit@wp.pl")
                .withRegisterDate(LocalDateTime.now())
                .withLastLogin(LocalDateTime.now())
                .withRole(UserRole.ROLE_USER)
                .build();

        AddressModel addressModel = AddressModel.Builder.anAddressModel()
                .withUserModel(user)
                .withAddDate(LocalDateTime.now())
                .withCity("tnt")
                .withStreet("pop")
                .withPhoneNumber(998998)
                .build();

        UserModel user2 = UserModel.Builder.anUserModel()
                .withFirstName("mark")
                .withLastName("zuck")
                .withEmail("markZuck@interia.pl")
                .withRegisterDate(LocalDateTime.now())
                .withLastLogin(LocalDateTime.now())
                .withRole(UserRole.ROLE_USER)
                .build();

        AddressModel addressModel2 = AddressModel.Builder.anAddressModel()
                .withUserModel(user2)
                .withAddDate(LocalDateTime.now())
                .withCity("ptp")
                .withStreet("nop")
                .withPhoneNumber(1111222)
                .build();

        user.setAddressModel(addressModel);
        user2.setAddressModel(addressModel2);

        userRepository.saveAll(Arrays.asList(user, user2));
    }

    @Test
    public void shouldGetUserListWithPagination() {

        //Given

        //When
        List<User> userList = userService.getAllUsers(1);

        //Then
        Assertions.assertThat(userList.size()).isEqualTo(2);

    }

    @Test
    public void shouldGetUserListWithPaginationAndFilter() {

        //Given

        //When
        List<User> userList = userService.usersFiltering(1, "zuc");

        //Then
        Assertions.assertThat(userList.size()).isEqualTo(1);
        Assertions.assertThat(userList.get(0).getLastName().contains("zuc")).isTrue();
    }

    @Test
    public void shouldGetUserProfile() {

        //Given
        List<UserModel> userList = userRepository.findAll();
        UserModel userForTest = userList.get(0);

        //When
        User user = userService.getUserProfile(userForTest.getId());

        //Then
        Assertions.assertThat(user.getFirstName().equals(userForTest.getFirstName())).isTrue();
        Assertions.assertThat(user.getLastName().equals(userForTest.getLastName())).isTrue();
        Assertions.assertThat(user.getEmail().equals(userForTest.getEmail())).isTrue();
    }

    @Test
    public void shouldSaveAddress() {

        //Given
        List<UserModel> userList = userRepository.findAll();
        User userForTest = userService.getUserProfile(userList.get(0).getId());

        //When
        Address addressForTest = userForTest.getAddress();
        addressForTest.setCity("long");
        addressForTest.setPhoneNumber("222333444");

        userService.saveAddress(userForTest, addressForTest);

        User userForCheck = userService.getUserProfile(userForTest.getId());

        //Then
        Assertions.assertThat(userForCheck.getAddress().getCity().equals(addressForTest.getCity())).isTrue();
        Assertions.assertThat(userForCheck.getAddress().getPhoneNumber().equals(addressForTest.getPhoneNumber())).isTrue();
    }

    @Test
    public void shouldSetLoginParametersAndGetBackUser() {

        //Given
        List<UserModel> userList = userRepository.findAll();
        User userForTest = userService.getUserProfile(userList.get(0).getId());

        //When
        userService.setLoginParamsAndGetBackUser(userForTest, "192.168.1.12");

        User userForCheck = userService.getUserProfile(userForTest.getId());

        //Then
        Assertions.assertThat(userForCheck.getIp().equals("192.168.1.12")).isTrue();
        Assertions.assertThat(userForCheck.getLastLogin().isAfter(userForTest.getLastLogin())).isTrue();
    }

    @Test
    public void shouldGetUserByEmail() {

        //Given
        List<UserModel> userList = userRepository.findAll();
        UserModel userForTest = userList.get(0);

        //When
        User userForCheck = userService.getUserByEmail(userForTest.getEmail());

        //Then
        Assertions.assertThat(userForCheck.getFirstName().equals(userForTest.getFirstName())).isTrue();
        Assertions.assertThat(userForCheck.getLastName().equals(userForTest.getLastName())).isTrue();
        Assertions.assertThat(userForCheck.getEmail().equals(userForTest.getEmail())).isTrue();
    }

    @Test
    public void shouldCheckUserExist() {

        //Given
        List<UserModel> userList = userRepository.findAll();
        UserModel userForTest = userList.get(0);

        //When
        boolean resultTrue = userService.userExist(userForTest.getEmail());
        boolean resultFalse = userService.userExist("bob@gmail.com");

        //Then
        Assertions.assertThat(resultTrue).isTrue();
        Assertions.assertThat(resultFalse).isFalse();
    }

    @Test
    public void shouldSaveUserWithAddressAndVerification() {

        //Given
        UserModel userForTest = UserModel.Builder.anUserModel()
                .withFirstName("anna")
                .withLastName("wal")
                .withEmail("annaWal@gmail.com")
                .withRegisterDate(LocalDateTime.now())
                .withLastLogin(LocalDateTime.now())
                .withRole(UserRole.ROLE_USER)
                .build();

        AddressModel addressModel = AddressModel.Builder.anAddressModel()
                .withAddDate(LocalDateTime.now())
                .withCity("ops")
                .withStreet("lops")
                .withPhoneNumber(999000111)
                .build();

        VerificationModel token = VerificationModel.Builder.aVerificationModel()
                .withToken("token")
                .withType(TokenTypes.ACTIVATION)
                .withExpiryDate(LocalDateTime.now().plusMinutes(1))
                .build();

        //When
        userService.saveUserWithAddressAndVerification(UserTransformer.getUserDtoFromModel(userForTest), AddressTransformer.getAddressDtoFromModel(addressModel), VerificationTransformer.getVerificationDtoFromModel(token));

        Optional<UserModel> userForCheckOptional = userRepository.findByEmail(userForTest.getEmail());
        UserModel userForCheck;

        userForCheck = userForCheckOptional.orElseGet(UserModel::new);

        //Then
        Assertions.assertThat(userForCheck.getFirstName().equals(userForTest.getFirstName())).isTrue();
        Assertions.assertThat(userForCheck.getLastName().equals(userForTest.getLastName())).isTrue();
        Assertions.assertThat(userForCheck.getEmail().equals(userForTest.getEmail())).isTrue();

        Assertions.assertThat(userForCheck.getAddressModel().getCity().equals(addressModel.getCity())).isTrue();
        Assertions.assertThat(userForCheck.getAddressModel().getPhoneNumber().equals(addressModel.getPhoneNumber())).isTrue();

        Assertions.assertThat(userForCheck.getVerificationModel().getToken().equals(token.getToken())).isTrue();
    }

    @Test
    public void shouldActivateUser() {

        //Given
        UserModel userForTest = UserModel.Builder.anUserModel()
                .withFirstName("anna")
                .withLastName("wal")
                .withEmail("annaWal@gmail.com")
                .withRegisterDate(LocalDateTime.now())
                .withLastLogin(LocalDateTime.now())
                .withRole(UserRole.ROLE_USER)
                .build();

        AddressModel addressModel = AddressModel.Builder.anAddressModel()
                .withAddDate(LocalDateTime.now())
                .withCity("ops")
                .withStreet("lops")
                .withPhoneNumber(999000111)
                .build();

        VerificationModel token = VerificationModel.Builder.aVerificationModel()
                .withToken("token")
                .withType(TokenTypes.ACTIVATION)
                .withExpiryDate(LocalDateTime.now().plusMinutes(1))
                .build();

        userService.saveUserWithAddressAndVerification(UserTransformer.getUserDtoFromModel(userForTest), AddressTransformer.getAddressDtoFromModel(addressModel), VerificationTransformer.getVerificationDtoFromModel(token));

        //When
        userService.activateUser("token", "password123$");

        Optional<UserModel> userForCheckOptional = userRepository.findByEmail("annaWal@gmail.com");
        UserModel userForCheck;

        userForCheck = userForCheckOptional.orElseGet(UserModel::new);

        //Then
        Assertions.assertThat(userForCheck.isEnabled()).isTrue();
        Assertions.assertThat(userForCheck.getPassword() != null).isTrue();
        Assertions.assertThat(userForCheck.getVerificationModel() == null).isTrue();
    }

    @Test
    public void shouldNotActivateUser() {

        //Given

        //When
        Throwable throwable = catchThrowable(() -> userService.activateUser("tokenAbc", "password$321"));

        //Then
        assertThat(throwable).isInstanceOf(TokenNotExistException.class);
    }
}