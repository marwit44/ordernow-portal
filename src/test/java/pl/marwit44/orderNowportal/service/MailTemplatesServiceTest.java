package pl.marwit44.orderNowportal.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.marwit44.orderNowportal.dto.MailDetails;
import pl.marwit44.orderNowportal.dto.MailTemplate;
import pl.marwit44.orderNowportal.exceptions.EditMailTemplateException;
import pl.marwit44.orderNowportal.model.MailTemplateBodyModel;
import pl.marwit44.orderNowportal.model.MailTemplateModel;
import pl.marwit44.orderNowportal.model.MailTypes;
import pl.marwit44.orderNowportal.repository.MailTemplateBodyRepository;
import pl.marwit44.orderNowportal.repository.MailTemplatesRepository;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@RunWith(SpringRunner.class)
@DataJpaTest
public class MailTemplatesServiceTest {

    private MailTemplatesService mailTemplatesService;

    @Autowired
    private MailTemplatesRepository mailTemplatesRepository;

    @Autowired
    private MailTemplateBodyRepository mailTemplateBodyRepository;

    @Before
    public void init() {

        mailTemplatesService = new MailTemplatesService(mailTemplatesRepository, mailTemplateBodyRepository);

        MailTemplateModel mailTemplateModel = MailTemplateModel.Builder.aMailTemplateModel()
                .withName("Register")
                .withType(MailTypes.REGISTER)
                .withCreationDate(LocalDateTime.now())
                .build();

        MailTemplateModel mailTemplateModel2 = MailTemplateModel.Builder.aMailTemplateModel()
                .withName("Reset password")
                .withType(MailTypes.PASSWORD_RESET)
                .withCreationDate(LocalDateTime.now())
                .build();

        MailTemplateBodyModel mailTemplateBodyModel = MailTemplateBodyModel.Builder.aMailTemplateBodyModel()
                .withMail(mailTemplateModel)
                .withTemplate("Registration email")
                .withCreationDate(LocalDateTime.now())
                .withLastModifyDate(LocalDateTime.now())
                .build();

        MailTemplateBodyModel mailTemplateBodyModel2 = MailTemplateBodyModel.Builder.aMailTemplateBodyModel()
                .withMail(mailTemplateModel2)
                .withTemplate("Reset password email")
                .withCreationDate(LocalDateTime.now())
                .withLastModifyDate(LocalDateTime.now())
                .build();

        mailTemplateModel.setMailTemplateBodyModel(mailTemplateBodyModel);
        mailTemplateModel2.setMailTemplateBodyModel(mailTemplateBodyModel2);

        mailTemplatesRepository.saveAll(Arrays.asList(mailTemplateModel, mailTemplateModel2));
    }

    @Test
    public void shouldGetAllMailsWithPagination() {

        //Given

        //When
        List<MailTemplate> mailTemplates = mailTemplatesService.getAllTemplates(1);

        //Then
        Assertions.assertThat(mailTemplates.size()).isEqualTo(2);
    }

    @Test
    public void shouldGetAllMailsWithPaginationAndFilter() {

        //Given

        //When
        List<MailTemplate> mailTemplates = mailTemplatesService.templatesFiltering(1, "Reg");

        //Then
        Assertions.assertThat(mailTemplates.size()).isEqualTo(1);
        Assertions.assertThat(mailTemplates.get(0).getName().contains("Reg")).isTrue();
    }

    @Test
    public void shouldGetMailTemplateDetail() {

        //Given
        List<MailTemplateModel> mailTemplates = mailTemplatesRepository.findAll();
        MailTemplateModel mailTemplateForTest = mailTemplates.get(0);

        //When
        MailDetails mailDetails = mailTemplatesService.getMailTemplateDetail(mailTemplateForTest.getId());

        //Then
        Assertions.assertThat(mailDetails.getName().equals(mailTemplateForTest.getName())).isTrue();
        Assertions.assertThat(mailDetails.getType() == mailTemplateForTest.getType()).isTrue();
    }

    @Test
    public void shouldEditMailTemplateDetail() {

        //Given
        List<MailTemplateModel> mailTemplates = mailTemplatesRepository.findAll();
        MailDetails mailDetailsForTest = mailTemplatesService.getMailTemplateDetail(mailTemplates.get(0).getId());

        //When
        mailDetailsForTest.setTemplate("Template for edit test");
        mailTemplatesService.editMailTemplate(mailDetailsForTest);

        MailDetails mailDetailsForCheck = mailTemplatesService.getMailTemplateDetail(mailDetailsForTest.getId());

        //Then
        Assertions.assertThat(mailDetailsForTest.getTemplate().equals(mailDetailsForCheck.getTemplate())).isTrue();
    }

    @Test
    public void shouldNotEditMailTemplateDetail() {

        //Given
        MailDetails mailDetailsForTest = new MailDetails();
        mailDetailsForTest.setId(1111L);

        //When
        Throwable throwable = catchThrowable(() -> mailTemplatesService.editMailTemplate(mailDetailsForTest));

        //Then
        assertThat(throwable).isInstanceOf(EditMailTemplateException.class);
    }

    @Test
    public void shouldGetMailTemplateDetailByType() {

        //Given

        //When
        MailDetails mailDetails = mailTemplatesService.getByType(MailTypes.REGISTER);

        //Then
        Assertions.assertThat(mailDetails.getName().equals("Register")).isTrue();
        Assertions.assertThat(mailDetails.getType() == MailTypes.REGISTER).isTrue();
    }
}