INSERT INTO MAIL_TEMPLATES(ID,NAME,TYPE,CREATION_DATE,LAST_MODIFY_DATE, COPY_TO, BLIND_COPY_TO, REPLY_TO, SUBJECT) VALUES
(nextval('mail_template_id_seq'),'Register email', 'REGISTER', current_timestamp, current_timestamp, 'marwit44@o2.pl', 'marwit44@o2.pl', 'marwit44@o2.pl', 'OrderNow registration');

INSERT INTO MAIL_TEMPLATES(ID,NAME,TYPE,CREATION_DATE,LAST_MODIFY_DATE, COPY_TO, BLIND_COPY_TO, REPLY_TO, SUBJECT) VALUES
(nextval('mail_template_id_seq'),'Reset password email', 'PASSWORD_RESET', current_timestamp, current_timestamp, 'marwit44@o2.pl', 'marwit44@o2.pl', 'marwit44@o2.pl', 'Reset password');