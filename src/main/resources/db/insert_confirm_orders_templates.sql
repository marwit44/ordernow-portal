INSERT INTO MAIL_TEMPLATES(ID,NAME,TYPE,CREATION_DATE,LAST_MODIFY_DATE, COPY_TO, BLIND_COPY_TO, REPLY_TO, SUBJECT) VALUES
(nextval('mail_template_id_seq'),'Confirm sending status', 'SENDING', current_timestamp, current_timestamp, 'marwit44@o2.pl', 'marwit44@o2.pl', 'marwit44@o2.pl', 'Order sent');

INSERT INTO MAIL_TEMPLATES(ID,NAME,TYPE,CREATION_DATE,LAST_MODIFY_DATE, COPY_TO, BLIND_COPY_TO, REPLY_TO, SUBJECT) VALUES
(nextval('mail_template_id_seq'),'Confirm ready to deliver status', 'READY_TO_DELIVER', current_timestamp, current_timestamp, 'marwit44@o2.pl', 'marwit44@o2.pl', 'marwit44@o2.pl', 'Order ready to deliver');

INSERT INTO MAIL_TEMPLATES(ID,NAME,TYPE,CREATION_DATE,LAST_MODIFY_DATE, COPY_TO, BLIND_COPY_TO, REPLY_TO, SUBJECT) VALUES
(nextval('mail_template_id_seq'),'Confirm done status', 'DONE', current_timestamp, current_timestamp, 'marwit44@o2.pl', 'marwit44@o2.pl', 'marwit44@o2.pl', 'Order done');
