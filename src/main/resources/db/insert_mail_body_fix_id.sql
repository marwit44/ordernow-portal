INSERT INTO MAIL_TEMPLATE_BODY(ID,TEMPLATE,CREATION_DATE,LAST_MODIFY_DATE,VERSION) VALUES (nextval('mail_template_body_id_seq'),
    '<h1 style="color: #5e9ca0;">Welcome ${template.name}!</h1>
    <h2 style="color: #2e6c80;">Your order is done</h2>
    <p>You paid: ${template.price}</p>
    <p>For items:</p>
    <p>${template.itemList}</p>
    <p>Greetings, team <strong> <span style="color: #ff0000;">OrderNow</span></strong>!</p>',
    TO_TIMESTAMP('2019-07-20','YYYY-MM-DD'),TO_TIMESTAMP('2019-07-20','YYYY-MM-DD'),0
    );