INSERT INTO MAIL_TEMPLATE_BODY(ID,TEMPLATE,CREATION_DATE,LAST_MODIFY_DATE,VERSION) VALUES (nextval('mail_template_body_id_seq'),
    '<h1 style="color: #5e9ca0;">Welcome ${template.name}!</h1>
    <h2 style="color: #2e6c80;">You are registered in OrderNow</h2>
    <p>To finish registration process click in link below:</p>
    <p>${template.activationLink}</p>
    <p>Greetings, team <strong> <span style="color: #ff0000;">OrderNow</span></strong>!</p>',
    TO_TIMESTAMP('2019-07-20','YYYY-MM-DD'),TO_TIMESTAMP('2019-07-20','YYYY-MM-DD'),0
    );

INSERT INTO MAIL_TEMPLATE_BODY(ID,TEMPLATE,CREATION_DATE,LAST_MODIFY_DATE,VERSION) VALUES (nextval('mail_template_body_id_seq'),
    '<h1 style="color: #5e9ca0;">Welcome ${template.name}</h1>
    <h2 style="color: #2e6c80;">Reset password message</h2>
    <p>To reset your password click in link below:</p>
    <p>${template.resetPasswordLnk}</p>
    <p>Greetings, team <strong> <span style="color: #ff0000;">OrderNow</span></strong>!</p>',
    TO_TIMESTAMP('2019-07-20','YYYY-MM-DD'),TO_TIMESTAMP('2019-07-20','YYYY-MM-DD'),0
    );
