        function closeWindow() {
            close();
        }

        function showAddItemModal() {
            $(".modal-body").empty();
            $(".modal-body").load("/items/add");
        }

        function showEditItemModal(id) {
            $('#titleLabelAddEdit').text("Edit");
            $(".modal-body").empty();
            $(".modal-body").load("/items/edit?id="+id);
        }

        function showDeleteItemModal(id) {
            $('#titleLabelAddEdit').text("Delete");
            $(".modal-body").empty();
            $(".modal-body").load("/items/delete?id="+id);
        }

        function showBuyItemModal(id) {
            $('#titleLabelAddEdit').text("Buy");
            $(".modal-body").empty();
            $(".modal-body").load("/items/buy?id="+id);
        }

        function showEditOrderModal(id) {
            $('#titleLabelDeleteAccept').text("Edit");
            $(".modal-body").empty();
            $(".modal-body").load("/orders/edit?id="+id);
        }

        function showDeleteOrderModal(id) {
            $('#titleLabelDeleteAccept').text("Delete");
            $(".modal-body").empty();
            $(".modal-body").load("/orders/delete?id="+id);
        }

        function showDeleteItemFromOrderModal(id) {
            $('#titleLabelDeleteAccept').text("Delete");
            $(".modal-body").empty();
            $(".modal-body").load("/itemInOrder/deleteItem?id="+id);
        }

        function showDeleteShoppingCardModal(id) {
            $('#titleLabelDeleteAccept').text("Delete");
            $(".modal-body").empty();
            $(".modal-body").load("/itemInOrder/delete?id="+id);
        }

        function showAcceptOrderModal(id) {
            $('#titleLabelDeleteAccept').text("Accept");
            $(".modal-body").empty();
            $(".modal-body").load("/orders/accept?id="+id);
        }

        function showUserDetailsModal(id) {
            $('#titleLabelDeleteAccept').text("User details");
            $(".modal-body").empty();
            $(".modal-body").load("/users/details?id="+id);
        }

        function showAddItemDetailsModal(id) {
            $(".modal-body").empty();
            $(".modal-body").load("/itemsDetails/addImage?id="+id);
        }

        function showDeleteItemDetailsModal(id,pictureNumber) {
            $('#titleLabelAddDeleteImage').text("Delete");
            $(".modal-body").empty();
            $(".modal-body").load("/itemsDetails/deleteImage?id="+id+"&pictureNumber="+pictureNumber);
        }

        $(function() {
            /*  Submit form using Ajax */
            $('button[name=submitAddItemButton]').click(function(e) {

                //Prevent default submission of form
                e.preventDefault();

                //Remove all errors
                $('.error').remove();

                $.post({
                    url : '/items/save',
                    data : $('form[name=addItemForm]').serialize(),
                    success : function(res) {

                        if(res.validated){
                            $('#addEditItemModal').hide();
                            $('.modal-backdrop').hide();

                            $(location).attr('href', '/items')

                         }else{
                            //Set error messages
                            $.each(res.errorMessages,function(key,valueList){
                                $.each(valueList, function( index, value ) {
                                    $('input[name='+key+']').after('<span class="error">'+value+'</br></span>');
                                 });
                            });
                         }
                     }
                 })
            });
        });

        $(function() {
            /*  Submit form using Ajax */
            $('button[name=submitEditItemButton]').click(function(e) {

                //Prevent default submission of form
                e.preventDefault();

                //Remove all errors
                $('.error').remove();

                $.post({
                    url : '/items/saveEditItem',
                    data : $('form[name=editItemForm]').serialize(),
                    success : function(res) {

                        if(res.validated){
                            $('#itemModal').hide();
                            $('.modal-backdrop').hide();

                            $(location).attr('href', '/items')

                        }else{
                            //Set error messages
                            $.each(res.errorMessages,function(key,valueList){
                                $.each(valueList, function( index, value ) {
                                    $('input[name='+key+']').after('<span class="error">'+value+'</br></span>');
                                });
                            });
                        }
                    }
                })
            });
        });

        $(function() {
            /*  Submit form using Ajax */
            $('button[name=submitBuyItemButton]').click(function(e) {

                //Prevent default submission of form
                e.preventDefault();

                //Remove all errors
                $('.error').remove();

                $.post({
                    url : '/items/buy',
                    data : $('form[name=buyItemForm]').serialize(),
                    success : function(res) {

                        if(res.validated){
                            $('#itemModal').hide();
                            $('.modal-backdrop').hide();

                            $(location).attr('href', '/items')

                        }else{
                            //Set error messages
                            $.each(res.errorMessages,function(key,valueList){
                                $.each(valueList, function( index, value ) {
                                    $('input[name='+key+']').after('<span class="error">'+value+'</br></span>');
                                });
                            });
                        }
                    }
                })
            });
        });

        $(function() {
            /*  Submit form using Ajax */
            $('button[name=submitDeleteItemFromOrderButton]').click(function(e) {

                //Prevent default submission of form
                e.preventDefault();

                //Remove all errors
                $('.error').remove();

                $.post({
                    url : '/itemInOrder/deleteItem',
                    data : $('form[name=deleteItemFromOrderForm]').serialize(),
                    success : function(res) {

                        if(res.validated){
                            $('#deleteAcceptOrderModal').hide();
                            $('.modal-backdrop').hide();

                            $(location).attr('href', '/itemInOrder/shoppingCard')

                        }else{
                            //Set error messages
                            $.each(res.errorMessages,function(key,valueList){
                                $.each(valueList, function( index, value ) {
                                    $('input[name='+key+']').after('<span class="error">'+value+'</br></span>');
                                });
                            });
                        }
                    }
                })
            });
        });