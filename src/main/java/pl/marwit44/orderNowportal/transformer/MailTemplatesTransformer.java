package pl.marwit44.orderNowportal.transformer;

import pl.marwit44.orderNowportal.dto.MailDetails;
import pl.marwit44.orderNowportal.dto.MailTemplate;
import pl.marwit44.orderNowportal.model.MailTemplateBodyModel;
import pl.marwit44.orderNowportal.model.MailTemplateModel;

public class MailTemplatesTransformer {

    public static MailTemplate getMailTemplateDto(MailTemplateModel mailTemplateModel) {
        MailTemplate mailTemplate = new MailTemplate();
        mailTemplate.setId(mailTemplateModel.getId());
        mailTemplate.setName(mailTemplateModel.getName());
        mailTemplate.setMailType(mailTemplateModel.getType());
        mailTemplate.setCreateTime(mailTemplateModel.getCreationDate());
        mailTemplate.setUpdateTime(mailTemplateModel.getLastModifyDate());
        mailTemplate.setVersion(mailTemplateModel.getVersion());
        return mailTemplate;
    }

    public static MailDetails getMailDetailsDto(MailTemplateBodyModel mailTemplateBodyModel) {
        MailDetails mailDetails = new MailDetails();
        mailDetails.setId(mailTemplateBodyModel.getId());
        mailDetails.setName(mailTemplateBodyModel.getMail().getName());
        mailDetails.setTemplate(mailTemplateBodyModel.getTemplate());
        mailDetails.setCopyTo(mailTemplateBodyModel.getMail().getCopyTo());
        mailDetails.setBlindCopyTo(mailTemplateBodyModel.getMail().getBlindCopyTo());
        mailDetails.setReplyTo(mailTemplateBodyModel.getMail().getReplyTo());
        mailDetails.setSubject(mailTemplateBodyModel.getMail().getSubject());
        mailDetails.setType(mailTemplateBodyModel.getMail().getType());
        return mailDetails;
    }
}
