package pl.marwit44.orderNowportal.transformer;

import pl.marwit44.orderNowportal.dto.Address;
import pl.marwit44.orderNowportal.model.AddressModel;

public class AddressTransformer {

    public static Address getAddressDtoFromModel(AddressModel addressModel) {
        Address address = new Address();
        address.setId(addressModel.getId());
        address.setStreet(addressModel.getStreet());
        address.setCity(addressModel.getCity());
        address.setPhoneNumber("" + addressModel.getPhoneNumber());
        address.setAddDate(addressModel.getAddDate());
        address.setUpdateDate(addressModel.getUpdateDate());
        address.setVersion(addressModel.getVersion());
        return address;
    }

    public static AddressModel getAddressModelFromDto(Address address) {
        AddressModel addressModel = new AddressModel();
        addressModel.setId(address.getId());
        addressModel.setStreet(address.getStreet());
        addressModel.setCity(address.getCity());
        addressModel.setPhoneNumber(Integer.parseInt(address.getPhoneNumber()));
        addressModel.setAddDate(address.getAddDate());
        addressModel.setUpdateDate(address.getUpdateDate());
        addressModel.setVersion(address.getVersion());
        return addressModel;
    }
}
