package pl.marwit44.orderNowportal.transformer;

import pl.marwit44.orderNowportal.dto.Verification;
import pl.marwit44.orderNowportal.model.VerificationModel;

public class VerificationTransformer {

    public static VerificationModel getVerificationModelFromDto(Verification verification){
        VerificationModel verificationModel = new VerificationModel();
        verificationModel.setId(verification.getId());
        verificationModel.setToken(verification.getToken());
        verificationModel.setExpiryDate(verification.getExpiryDate());
        verificationModel.setType(verification.getType());
        verificationModel.setUpdateDate(verification.getUpdateDate());
        verificationModel.setVersion(verification.getVersion());
        return verificationModel;
    }

    public static Verification getVerificationDtoFromModel(VerificationModel verificationModel) {
        Verification verification = new Verification();
        verification.setId(verificationModel.getId());
        verification.setToken(verificationModel.getToken());
        verification.setExpiryDate(verificationModel.getExpiryDate());
        verification.setType(verificationModel.getType());
        verification.setCreationDate(verificationModel.getCreationDate());
        verification.setUpdateDate(verificationModel.getUpdateDate());
        verification.setVersion(verificationModel.getVersion());
        return verification;
    }
}
