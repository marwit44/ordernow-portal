package pl.marwit44.orderNowportal.transformer;

import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.model.UserModel;

public class UserTransformer {

    public static User getUserDtoFromModel(UserModel userModel) {
        User user = new User();
        user.setId(userModel.getId());
        user.setFirstName(userModel.getFirstName());
        user.setLastName(userModel.getLastName());
        user.setEmail(userModel.getEmail());
        user.setRole(userModel.getRole());
        user.setEnabled(userModel.getEnabled());
        user.setPassword(userModel.getPassword());
        user.setLastLogin(userModel.getLastLogin());
        user.setIp(userModel.getIp());
        user.setRegisterDate(userModel.getRegisterDate());
        user.setUpdateDate(userModel.getUpdateDate());
        user.setVersion(userModel.getVersion());
        return user;
    }

    public static User getUserDtoFromModelWithAddress(UserModel userModel) {
        User user = getUserDtoFromModel(userModel);
        user.setAddress(AddressTransformer.getAddressDtoFromModel(userModel.getAddressModel()));
        return user;
    }

    public static UserModel getUserModelFromDto(User user) {
        UserModel userModel = new UserModel();
        userModel.setId(user.getId());
        userModel.setFirstName(user.getFirstName());
        userModel.setLastName(user.getLastName());
        userModel.setEmail(user.getEmail());
        userModel.setRole(user.getRole());
        userModel.setEnabled(user.getEnabled());
        userModel.setPassword(user.getPassword());
        userModel.setLastLogin(user.getLastLogin());
        userModel.setIp(user.getIp());
        userModel.setUpdateDate(user.getUpdateDate());
        userModel.setVersion(user.getVersion());
        return userModel;
    }

    public static UserModel getUserModelFromDtoWithAddress(User user) {
        UserModel userModel = getUserModelFromDto(user);
        userModel.setAddressModel(AddressTransformer.getAddressModelFromDto(user.getAddress()));
        return userModel;
    }

    public static User getUserDtoFromModelWithVerification(UserModel userModel) {
        User user = getUserDtoFromModel(userModel);
        if(userModel.getVerificationModel() != null) {
            user.setVerification(VerificationTransformer.getVerificationDtoFromModel(userModel.getVerificationModel()));
        }

        return user;
    }

    public static UserModel getUserModelFromDtoWithVerification(User user) {
        UserModel userModel = getUserModelFromDto(user);
        if(user.getVerification() != null) {
            userModel.setVerificationModel(VerificationTransformer.getVerificationModelFromDto(user.getVerification()));
        }

        return userModel;
    }
}
