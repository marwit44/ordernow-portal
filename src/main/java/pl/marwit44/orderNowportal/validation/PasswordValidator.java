package pl.marwit44.orderNowportal.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import pl.marwit44.orderNowportal.form.PasswordForm;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class PasswordValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass== PasswordForm.class;
    }

    @Override
    public void validate(Object o, Errors errors) {
        PasswordForm form = (PasswordForm) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"password","Field.required","Field must be not empty and without whitespace");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"confirmPassword","Field.required","Field must be not empty and without whitespace");

        Pattern pattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(form.getPassword());

        if(form.getPassword().length()<8) {
            errors.rejectValue("password","Password.minLong","Minimum size of password is 8 characters");
        }

        if (form.getPassword().length()>255) {
            errors.rejectValue("password","Password.maxLong","Maximum size of password is 255 characters");
        }

        if (form.getPassword().equals(form.getPassword().toLowerCase())) {
            errors.rejectValue("password","Password.lowerCase", "Password need to contain minimum one uppercase letter");
        }

        if (form.getPassword().equals(form.getPassword().toUpperCase())) {
            errors.rejectValue("password","Password.upperCase","Password need to contain minimum one lowercase letter.");
        }

        if (!matcher.find()) {
            errors.rejectValue("password","Password.specialChar","Password need to contain 1 special character.");
        }

        if(!form.getPassword().equals(form.getConfirmPassword())) {
            errors.rejectValue("confirmPassword","Password.noMatch", "Passwords not match");
        }
    }
}
