package pl.marwit44.orderNowportal.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.marwit44.orderNowportal.exceptions.ItemNotFoundException;
import pl.marwit44.orderNowportal.service.ItemService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueItemNameValidator implements ConstraintValidator<UniqueItemName, String> {
    private final ItemService itemService;

    private final Logger log = LoggerFactory.getLogger(UniqueItemNameValidator.class);

    public UniqueItemNameValidator(ItemService itemService) {
        this.itemService = itemService;
    }

    @Override
    public void initialize(UniqueItemName constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        try {
            if (itemService.getItemByName(value) != null) {
                return false;
            }
        } catch (ItemNotFoundException e) {
            log.error(e.getMessage());
            return true;
        }
        return false;
    }
}
