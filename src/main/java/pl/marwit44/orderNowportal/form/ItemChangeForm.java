package pl.marwit44.orderNowportal.form;

import pl.marwit44.orderNowportal.dto.Token;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ItemChangeForm {

    private Long id;

    @Digits(integer = 19, fraction = 0, message = "must be only numbers")
    private int amount;

    @NotBlank
    @Size(max = 255)
    private String name;

    @Digits(integer = 19, fraction = 2, message = "must be only numbers with maximum 2 digits after decimal point")
    private double price;

    private Token token;

    public ItemChangeForm(ItemForm itemForm) {
        if(itemForm != null) {
            this.id = itemForm.getId();
            this.name = itemForm.getName();
            this.amount = itemForm.getAmount();
            this.price = itemForm.getPrice();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }
}
