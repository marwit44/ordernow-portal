package pl.marwit44.orderNowportal.model;

public enum UserRole {
    ROLE_USER,
    ROLE_ADMIN
}
