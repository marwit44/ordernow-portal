package pl.marwit44.orderNowportal.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import java.time.LocalDateTime;

@Entity
@Table(name = "VERIFICATION")
public class VerificationModel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "verification_generator")
    @SequenceGenerator(name = "verification_generator", sequenceName = "verification_id_seq", allocationSize = 1)
    private Long id;

    private String token;

    private LocalDateTime expiryDate;

    @Enumerated(EnumType.STRING)
    private TokenTypes type;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID", nullable = false)
    private UserModel userModel;

    @CreationTimestamp
    @Column(name = "creation_date", nullable = false, updatable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Version
    private Long version = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public TokenTypes getType() {
        return type;
    }

    public void setType(TokenTypes type) {
        this.type = type;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }


    public static final class Builder {
        UserModel userModel;
        private String token;
        private LocalDateTime expiryDate;
        private TokenTypes type;

        private Builder() {
        }

        public static Builder aVerificationModel() {
            return new Builder();
        }

        public Builder withToken(String token) {
            this.token = token;
            return this;
        }

        public Builder withExpiryDate(LocalDateTime expiryDate) {
            this.expiryDate = expiryDate;
            return this;
        }

        public Builder withType(TokenTypes type) {
            this.type = type;
            return this;
        }

        public VerificationModel build() {
            VerificationModel verificationModel = new VerificationModel();
            verificationModel.setToken(token);
            verificationModel.setExpiryDate(expiryDate);
            verificationModel.setType(type);
            verificationModel.setUserModel(userModel);
            Long version = 0L;
            verificationModel.setVersion(version);
            return verificationModel;
        }
    }
}
