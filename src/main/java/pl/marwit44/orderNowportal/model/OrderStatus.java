package pl.marwit44.orderNowportal.model;

public enum OrderStatus {

    PREPARATION(0),
    SENDING(1),
    READY_TO_DELIVER(2),
    DONE(3);

    private final int value;

    OrderStatus(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }
}
