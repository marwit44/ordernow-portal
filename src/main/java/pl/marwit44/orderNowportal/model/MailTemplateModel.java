package pl.marwit44.orderNowportal.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import java.time.LocalDateTime;

@Entity
@Table(name = "MAIL_TEMPLATES")
public class MailTemplateModel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mail_template_seq_generator")
    @SequenceGenerator(name = "mail_template_seq_generator", sequenceName = "mail_template_id_seq", allocationSize = 1)
    private Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private MailTypes type;

    @CreationTimestamp
    @Column(name = "creation_date", nullable = false, updatable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime creationDate;

    @UpdateTimestamp
    private LocalDateTime lastModifyDate;

    @Version
    private Long version = 0L;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "mail", cascade = CascadeType.ALL)
    private MailTemplateBodyModel mailTemplateBodyModel;

    private String copyTo;

    private String blindCopyTo;

    private String replyTo;

    private String subject;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MailTypes getType() {
        return type;
    }

    public void setType(MailTypes type) {
        this.type = type;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(LocalDateTime lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public MailTemplateBodyModel getMailTemplateBodyModel() {
        return mailTemplateBodyModel;
    }

    public void setMailTemplateBodyModel(MailTemplateBodyModel mailTemplateBodyModel) {
        this.mailTemplateBodyModel = mailTemplateBodyModel;
    }

    public String getCopyTo() {
        return copyTo;
    }

    public void setCopyTo(String copyTo) {
        this.copyTo = copyTo;
    }

    public String getBlindCopyTo() {
        return blindCopyTo;
    }

    public void setBlindCopyTo(String blindCopyTo) {
        this.blindCopyTo = blindCopyTo;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }


    public static final class Builder {
        private String name;
        private MailTypes type;
        private LocalDateTime creationDate;

        private Builder() {
        }

        public static Builder aMailTemplateModel() {
            return new Builder();
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withType(MailTypes type) {
            this.type = type;
            return this;
        }

        public Builder withCreationDate(LocalDateTime creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public MailTemplateModel build() {
            MailTemplateModel mailTemplateModel = new MailTemplateModel();
            mailTemplateModel.setName(name);
            mailTemplateModel.setType(type);
            mailTemplateModel.setCreationDate(creationDate);
            Long version = 0L;
            mailTemplateModel.setVersion(version);
            return mailTemplateModel;
        }
    }
}
