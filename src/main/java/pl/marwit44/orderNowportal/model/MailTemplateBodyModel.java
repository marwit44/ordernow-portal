package pl.marwit44.orderNowportal.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import java.time.LocalDateTime;

@Entity
@Table(name = "MAIL_TEMPLATE_BODY")
public class MailTemplateBodyModel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mail_template_body_seq_generator")
    @SequenceGenerator(name = "mail_template_body_seq_generator", sequenceName = "mail_template_body_id_seq", allocationSize = 1)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private MailTemplateModel mail;

    private String template;

    @CreationTimestamp
    @Column(name = "creation_date", nullable = false, updatable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime creationDate;

    @UpdateTimestamp
    private LocalDateTime lastModifyDate;

    @Version
    private Long version = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MailTemplateModel getMail() {
        return mail;
    }

    public void setMail(MailTemplateModel mail) {
        this.mail = mail;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifyDate() {
        return lastModifyDate;
    }

    public void setLastModifyDate(LocalDateTime lastModifyDate) {
        this.lastModifyDate = lastModifyDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }


    public static final class Builder {
        private MailTemplateModel mail;
        private String template;
        private LocalDateTime creationDate;
        private LocalDateTime lastModifyDate;

        private Builder() {
        }

        public static Builder aMailTemplateBodyModel() {
            return new Builder();
        }

        public Builder withMail(MailTemplateModel mail) {
            this.mail = mail;
            return this;
        }

        public Builder withTemplate(String template) {
            this.template = template;
            return this;
        }

        public Builder withCreationDate(LocalDateTime creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public Builder withLastModifyDate(LocalDateTime lastModifyDate) {
            this.lastModifyDate = lastModifyDate;
            return this;
        }

        public MailTemplateBodyModel build() {
            MailTemplateBodyModel mailTemplateBodyModel = new MailTemplateBodyModel();
            mailTemplateBodyModel.setMail(mail);
            mailTemplateBodyModel.setTemplate(template);
            mailTemplateBodyModel.setCreationDate(creationDate);
            mailTemplateBodyModel.setLastModifyDate(lastModifyDate);
            Long version = 0L;
            mailTemplateBodyModel.setVersion(version);
            return mailTemplateBodyModel;
        }
    }
}
