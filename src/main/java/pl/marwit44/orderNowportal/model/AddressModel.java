package pl.marwit44.orderNowportal.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "Address")
public class AddressModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_seq_generator")
    @SequenceGenerator(name = "address_seq_generator", sequenceName = "address_id_seq", allocationSize = 1)
    private Long id;

    private String street;

    private String city;

    @Column(name = "phone_number")
    private Integer phoneNumber;

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private UserModel userModel;

    @CreationTimestamp
    @Column(name = "add_date", nullable = false, updatable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime addDate;

    @UpdateTimestamp
    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Version
    private Long version = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }

    public LocalDateTime getAddDate() {
        return addDate;
    }

    public void setAddDate(LocalDateTime addDate) {
        this.addDate = addDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }


    public static final class Builder {
        private String street;
        private String city;
        private Integer phoneNumber;
        private UserModel userModel;
        private LocalDateTime addDate;

        private Builder() {
        }

        public static Builder anAddressModel() {
            return new Builder();
        }

        public Builder withStreet(String street) {
            this.street = street;
            return this;
        }

        public Builder withCity(String city) {
            this.city = city;
            return this;
        }

        public Builder withPhoneNumber(Integer phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Builder withUserModel(UserModel userModel) {
            this.userModel = userModel;
            return this;
        }

        public Builder withAddDate(LocalDateTime addDate) {
            this.addDate = addDate;
            return this;
        }

        public AddressModel build() {
            AddressModel addressModel = new AddressModel();
            addressModel.setStreet(street);
            addressModel.setCity(city);
            addressModel.setPhoneNumber(phoneNumber);
            addressModel.setUserModel(userModel);
            addressModel.setAddDate(addDate);
            Long version = 0L;
            addressModel.setVersion(version);
            return addressModel;
        }
    }
}
