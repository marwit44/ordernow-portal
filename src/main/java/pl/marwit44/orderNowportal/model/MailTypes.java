package pl.marwit44.orderNowportal.model;

public enum  MailTypes {
    REGISTER,
    PASSWORD_RESET,
    SENDING,
    READY_TO_DELIVER,
    DONE
}
