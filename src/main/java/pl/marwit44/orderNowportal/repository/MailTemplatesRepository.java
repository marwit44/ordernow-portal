package pl.marwit44.orderNowportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pl.marwit44.orderNowportal.model.MailTemplateModel;
import pl.marwit44.orderNowportal.model.MailTypes;

@Repository
public interface MailTemplatesRepository extends JpaRepository<MailTemplateModel, Long>, JpaSpecificationExecutor<MailTemplateModel> {

    MailTemplateModel findByType(MailTypes type);
}
