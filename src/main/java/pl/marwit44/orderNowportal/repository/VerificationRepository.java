package pl.marwit44.orderNowportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.marwit44.orderNowportal.model.VerificationModel;

@Repository
public interface VerificationRepository extends JpaRepository<VerificationModel, Long> {
    VerificationModel findByToken(String token);
}
