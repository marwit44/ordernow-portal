package pl.marwit44.orderNowportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pl.marwit44.orderNowportal.model.UserModel;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserModel, Long>, JpaSpecificationExecutor<UserModel> {

    @Override
    Optional<UserModel> findById(Long aLong);

    Optional<UserModel> findByEmail(String email);

    UserModel getOneByIdAndLastLogin(Long id, LocalDateTime lastLogin);

}
