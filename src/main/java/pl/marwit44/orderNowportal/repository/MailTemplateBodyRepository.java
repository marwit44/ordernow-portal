package pl.marwit44.orderNowportal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.marwit44.orderNowportal.model.MailTemplateBodyModel;

@Repository
public interface MailTemplateBodyRepository extends JpaRepository<MailTemplateBodyModel, Long> {

}
