package pl.marwit44.orderNowportal.specifications;

import org.springframework.data.jpa.domain.Specification;
import pl.marwit44.orderNowportal.model.MailTemplateModel;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class EmailSpecification implements Specification<MailTemplateModel> {

    private final String searchTerm;

    public EmailSpecification(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    private String containsPattern(String searchTerm) {
        return "%" + searchTerm.toLowerCase() + "%";
    }

    @Override
    public Predicate toPredicate(Root<MailTemplateModel> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if(searchTerm == null) {
            return null;
        }

        return criteriaBuilder.or(
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("name").as(String.class)), containsPattern(searchTerm)
                ),
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("type").as(String.class)), containsPattern(searchTerm)
                ),
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("creationDate").as(String.class)), containsPattern(searchTerm)
                ),
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("lastModifyDate").as(String.class)), containsPattern(searchTerm)
                )
        );
    }
}
