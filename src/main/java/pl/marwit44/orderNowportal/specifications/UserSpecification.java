package pl.marwit44.orderNowportal.specifications;

import org.springframework.data.jpa.domain.Specification;
import pl.marwit44.orderNowportal.model.UserModel;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class UserSpecification implements Specification<UserModel> {

    private final String searchTerm;

    public UserSpecification(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    private String containsPattern(String searchTerm) {
        return "%" + searchTerm.toLowerCase() + "%";
    }

    @Override
    public Predicate toPredicate(Root<UserModel> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if (searchTerm == null) {
            return null;
        }

        return criteriaBuilder.or(
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("firstName").as(String.class)), containsPattern(searchTerm)
                ),
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("lastName").as(String.class)), containsPattern(searchTerm)
                ),
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("email").as(String.class)), containsPattern(searchTerm)
                ),
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("registerDate").as(String.class)), containsPattern(searchTerm)
                )
        );
    }
}
