package pl.marwit44.orderNowportal.service;

import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import pl.marwit44.orderNowportal.dto.Address;
import pl.marwit44.orderNowportal.dto.MailDetails;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.dto.Verification;
import pl.marwit44.orderNowportal.exceptions.SendRestartEmailException;
import pl.marwit44.orderNowportal.exceptions.TokenNotExistException;
import pl.marwit44.orderNowportal.exceptions.UserNotFoundException;
import pl.marwit44.orderNowportal.model.AddressModel;
import pl.marwit44.orderNowportal.model.MailTypes;
import pl.marwit44.orderNowportal.model.TokenTypes;
import pl.marwit44.orderNowportal.model.UserModel;
import pl.marwit44.orderNowportal.model.UserRole;
import pl.marwit44.orderNowportal.model.VerificationModel;
import pl.marwit44.orderNowportal.repository.UserRepository;
import pl.marwit44.orderNowportal.repository.VerificationRepository;
import pl.marwit44.orderNowportal.security.CustomUserDetails;
import pl.marwit44.orderNowportal.specifications.UserSpecification;
import pl.marwit44.orderNowportal.transformer.AddressTransformer;
import pl.marwit44.orderNowportal.transformer.UserTransformer;
import pl.marwit44.orderNowportal.transformer.VerificationTransformer;

import javax.mail.MessagingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class UserService implements UserDetailsService {

    @Value("${application.website}")
    private String websiteAddress;

    private final long EXPIRATION = 10080;

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private final VerificationRepository verificationRepository;

    private final MailingService mailingService;
    private final MailTemplatesService mailTemplatesService;

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    public UserService(UserRepository userRepository, VerificationRepository verificationRepository, MailingService mailingService, MailTemplatesService mailTemplatesService) {
        this.userRepository = userRepository;
        this.verificationRepository = verificationRepository;
        this.mailingService = mailingService;
        this.mailTemplatesService = mailTemplatesService;
    }

    public void getListItemWithPaginationAndFiltering(Model model, Integer pageId, String searchTerm) throws UnsupportedEncodingException {

        int pageNumber = ((pageId != null && pageId > 0) ? pageId : 1);

        List<User> allUsers;

        if (searchTerm != null && !searchTerm.equals("")) {
            allUsers = usersFiltering(pageNumber, URLDecoder.decode(searchTerm, "UTF-8"));
        } else {
            allUsers = getAllUsers(pageNumber);
        }

        int totalPages = 1;

        if (allUsers.size() > 0) {
            totalPages = allUsers.get(0).getTotalPages();
        }

        if (totalPages < pageNumber) {
            pageNumber = totalPages;
        }

        if (totalPages > 0) {

            int previousPages = pageNumber - 3;
            int nextPages = pageNumber + 3;

            if (previousPages < 1) {
                previousPages = 1;
            }

            if (nextPages > totalPages) {
                nextPages = totalPages;
            }

            List<Integer> pages = IntStream.rangeClosed(previousPages, nextPages).boxed().collect(Collectors.toList());

            model.addAttribute("pageNumber", pages);
        } else {
            List<Integer> pages = Collections.singletonList(1);
            model.addAttribute("pageNumber", pages);
        }

        model.addAttribute("actualPage", pageNumber);
        model.addAttribute("allUsers", allUsers);
        if (searchTerm != null) {
            model.addAttribute("searchTerm", searchTerm);
        } else {
            model.addAttribute("searchTerm", "");
        }
    }

    List<User> getAllUsers(int pageNumber) {

        try {

            long allUsersSizePages = (userRepository.count() - 1) / 10 + 1;

            if (pageNumber > (int) allUsersSizePages) {
                pageNumber = (int) allUsersSizePages;
            }

            if (pageNumber <= 0) {
                pageNumber = 1;
            }

            PageRequest pageable = PageRequest.of(pageNumber - 1, 10, Sort.by("lastName"));

            Page<UserModel> all = userRepository.findAll(pageable);

            List<User> userList = all.getContent().stream()
                    .map(UserTransformer::getUserDtoFromModel)
                    .collect(Collectors.toList());

            if (userList.size() > 0) {
                userList.get(0).setTotalPages(all.getTotalPages());
            }

            return userList;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ArrayList<>();
        }

    }

    List<User> usersFiltering(int pageNumber, String searchTerm) {

        UserSpecification userSpecification = new UserSpecification(searchTerm);

        long allUsersSizePages = (userRepository.count(userSpecification) - 1) / 10 + 1;

        if (pageNumber > (int) allUsersSizePages) {
            pageNumber = (int) allUsersSizePages;
        }

        if (pageNumber <= 0) {
            pageNumber = 1;
        }

        PageRequest pageable = PageRequest.of(pageNumber - 1, 10, Sort.by("lastName"));

        Page<UserModel> all = userRepository.findAll(userSpecification, pageable);

        List<User> userList = all.getContent().stream()
                .map(UserTransformer::getUserDtoFromModel)
                .collect(Collectors.toList());

        if (userList.size() > 0) {
            userList.get(0).setTotalPages(all.getTotalPages());
        }

        return userList;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserModel> userModel = userRepository.findByEmail(username);
        if (!userModel.isPresent()) {
            throw new UsernameNotFoundException("Username and or password was incorrect");
        }
        return new CustomUserDetails(userModel.get());
    }

    public User getLoggedUser() throws UserNotFoundException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        Optional<UserModel> userOptional = userRepository.findByEmail(username);

        if (userOptional.isPresent()) {

            UserModel userModel = userOptional.get();

            return UserTransformer.getUserDtoFromModelWithAddress(userModel);
        } else {
            throw new UserNotFoundException("User " + username + " not found");
        }
    }

    public User getUserProfile(Long id) {

        return UserTransformer.getUserDtoFromModelWithAddress(userRepository.getOne(id));
    }

    public void saveAddress(User user, Address address) {

        UserModel userModel = UserTransformer.getUserModelFromDtoWithAddress(user);

        AddressModel addressModel = userModel.getAddressModel();

        addressModel.setCity(address.getCity());
        addressModel.setStreet(address.getStreet());
        addressModel.setPhoneNumber(Integer.parseInt(address.getPhoneNumber()));

        userModel.setAddressModel(addressModel);

        userRepository.save(userModel);
    }

    public User setLoginParamsAndGetBackUser(User user, String ip) {

        UserModel userModel = UserTransformer.getUserModelFromDtoWithAddress(user);

        userModel.setLastLogin(LocalDateTime.now());
        userModel.setIp(ip);

        userRepository.save(userModel);

        return UserTransformer.getUserDtoFromModelWithAddress(userModel);
    }

    public User getUserByEmail(String email) {

        Optional<UserModel> userModel = userRepository.findByEmail(email);
        return userModel.map(UserTransformer::getUserDtoFromModelWithVerification).orElse(null);
    }

    public User getUserById(Long id) {

        Optional<UserModel> userModel = userRepository.findById(id);
        return userModel.map(UserTransformer::getUserDtoFromModelWithVerification).orElse(null);
    }

    @Transactional
    public User createUser(User user, Address address) throws MessagingException, IOException, TemplateException {

        if (userExist(user.getEmail())) {
            throw new RuntimeException("User with given email already exists");
        }

        String confirmationToken = UUID.randomUUID().toString();
        Map<String, Object> userEmailInfo = new HashMap<>();
        userEmailInfo.put("name", user.getFirstName() + " " + user.getLastName());
        userEmailInfo.put("activationLink", "http://" + websiteAddress + "/users/activate/" + confirmationToken);
        MailDetails mailDetailsDto = mailTemplatesService.getByType(MailTypes.REGISTER);

        Verification verification = Verification.Builder.aVerification()
                .withToken(confirmationToken)
                .withExpiryDate(LocalDateTime.now().plusMinutes(EXPIRATION))
                .withType(TokenTypes.ACTIVATION)
                .build();
        mailingService.sendEmail(user.getEmail(), mailDetailsDto, userEmailInfo);
        saveUserWithAddressAndVerification(user, address, verification);

        return user;
    }

    @Transactional
    public void sendRestartEmail(String email) {

        try {
            User user = getUserByEmail(email);

            if(user != null) {

                MailDetails mailDetailsDto = mailTemplatesService.getByType(MailTypes.PASSWORD_RESET);
                String token = UUID.randomUUID().toString();
                Map<String, Object> userEmailInfo = new HashMap<>();
                userEmailInfo.put("name", user.getFirstName() + " " + user.getLastName());
                userEmailInfo.put("resetPasswordLnk", "http://" + websiteAddress + "/users/restart_password/" + token);
                Verification verificationDto = Verification.Builder.aVerification()
                        .withToken(token)
                        .withExpiryDate(LocalDateTime.now().plusMinutes(EXPIRATION))
                        .withType(TokenTypes.RESTART)
                        .build();
                saveUserWithAddressAndVerification(user, null, verificationDto);
                mailingService.sendEmail(user.getEmail(), mailDetailsDto, userEmailInfo);
            }
        } catch (SendRestartEmailException | MessagingException | IOException | TemplateException e){
            throw new SendRestartEmailException("Failed to send restart email. Try again");
        }
    }

    boolean userExist(String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    void saveUserWithAddressAndVerification(User user, Address address, Verification verification) {

        UserModel userModel = UserTransformer.getUserModelFromDtoWithVerification(user);

        AddressModel addressModel = null;
        if(address != null) {
            addressModel = AddressTransformer.getAddressModelFromDto(address);
        }

        VerificationModel verificationModel = VerificationTransformer.getVerificationModelFromDto(verification);

        userModel.setEnabled(false);
        userModel.setRole(UserRole.ROLE_USER);

        if(userModel.getVerificationModel() != null) {

            VerificationModel verificationModelExist = userModel.getVerificationModel();
            verificationModelExist.setToken(verificationModel.getToken());
            verificationModelExist.setExpiryDate(verificationModel.getExpiryDate());
            verificationModelExist.setType(verificationModel.getType());

            verificationModelExist.setUserModel(userModel);
            userModel.setVerificationModel(verificationModelExist);

        } else {
            userModel.setVerificationModel(verificationModel);
            verificationModel.setUserModel(userModel);
        }

        if(addressModel != null) {
            userModel.setAddressModel(addressModel);
            addressModel.setUserModel(userModel);
        }

        userRepository.save(userModel);
    }

    @Transactional
    public void activateUser(String token, String pass) throws TokenNotExistException {

        VerificationModel verificationModel = verificationRepository.findByToken(token);

        if (verificationModel != null) {
            UserModel user = verificationModel.getUserModel();

            user.setEnabled(true);

            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            String hashedPassword = passwordEncoder.encode(pass);
            user.setPassword(hashedPassword);
            user.setVerificationModel(null);
            verificationRepository.delete(verificationModel);
            userRepository.save(user);

        } else {
            throw new TokenNotExistException("Token does not exist or has been already used.");
        }
    }

    public boolean isTokenCorrect(String token, TokenTypes type) {

        VerificationModel verificationModel = verificationRepository.findByToken(token);

        return verificationModel != null && verificationModel.getExpiryDate().isAfter(LocalDateTime.now()) && verificationModel.getType()==type;
    }

    public User getUserFromToken(String token) throws UserNotFoundException {

        VerificationModel verificationModel = verificationRepository.findByToken(token);

        if (verificationModel != null) {
            User user = UserTransformer.getUserDtoFromModel(verificationModel.getUserModel());
            user.setVerification(VerificationTransformer.getVerificationDtoFromModel(verificationModel));

            return user;
        } else {
            throw new UserNotFoundException("No user assigned to token.");
        }
    }
}
