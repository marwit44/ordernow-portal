package pl.marwit44.orderNowportal.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.marwit44.orderNowportal.dto.Item;
import pl.marwit44.orderNowportal.dto.Token;
import pl.marwit44.orderNowportal.exceptions.ItemAmountNotEnoughException;
import pl.marwit44.orderNowportal.exceptions.ItemNotFoundException;
import pl.marwit44.orderNowportal.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowportal.form.ItemChangeForm;
import pl.marwit44.orderNowportal.form.ItemForm;
import pl.marwit44.orderNowportal.model.UserRole;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ItemService {

    private final AuthorizationService authorizationService;
    private final CookieModelService cookieModelService;

    private final Logger log = LoggerFactory.getLogger(ItemService.class);

    @Value("${application.backendAddress}")
    private String backendAddress;

    public ItemService(AuthorizationService authorizationService, CookieModelService cookieModelService) {
        this.authorizationService = authorizationService;
        this.cookieModelService = cookieModelService;
    }

    public void getListItemWithPaginationAndFiltering(Model model, Integer pageId, String searchTerm, String sortBy, boolean desc,
                                                      int rangeFrom, int rangeTo, boolean changeFrom, boolean changeTo) throws UnsupportedEncodingException {
        int pageNumber = ((pageId != null && pageId > 0) ? pageId : 1);

        List<Item> allItems;

        if (rangeFrom > rangeTo && rangeFrom > -1 && rangeTo > -1) {
            int rangeTmp = rangeFrom;
            rangeFrom = rangeTo;
            rangeTo = rangeTmp;
        }

        if (searchTerm != null && !searchTerm.equals("")) {
            allItems = itemsFiltering(pageNumber, searchTerm, sortBy, desc, rangeFrom, rangeTo);
        } else {
            allItems = getAllItems(pageNumber, sortBy, desc, rangeFrom, rangeTo);
        }

        int totalPages = 1;

        if (allItems.size() > 0) {
            totalPages = allItems.get(0).getTotalPages();
        }

        cookieModelService.setBasicModelParameters(model, searchTerm, sortBy, desc, rangeFrom, rangeTo, changeFrom, changeTo, pageNumber, totalPages);

        model.addAttribute("allItems", allItems);
    }

    private List<Item> getAllItems(int page, String sortBy, boolean desc, int rangeFrom, int rangeTo) {

        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "item/getAll";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("page", page)
                .queryParam("sortBy", sortBy)
                .queryParam("desc", desc)
                .queryParam("rangeFrom", rangeFrom)
                .queryParam("rangeTo", rangeTo);

        ResponseEntity<List<Item>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Item>>() {
                });

        return response.getBody();
    }

    private List<Item> itemsFiltering(int page, String name, String sortBy, boolean desc, int rangeFrom, int rangeTo) {

        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "item/filtering";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("name", name)
                .queryParam("page", page)
                .queryParam("sortBy", sortBy)
                .queryParam("desc", desc)
                .queryParam("rangeFrom", rangeFrom)
                .queryParam("rangeTo", rangeTo);

        ResponseEntity<List<Item>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Item>>() {
                });

        return response.getBody();
    }

    public ItemForm getItemById(Long id) throws ItemNotFoundException {

        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "item/getById";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("id", id);

        try {
            ResponseEntity<ItemForm> response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<ItemForm>() {
                    });

            return response.getBody();
        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);
            throw new ItemNotFoundException("Item not found id: " + id);
        }
    }

    public void saveItem(ItemForm item) throws TokenAuthorizationException {

        try {

            Token token = authorizationService.getTokenObject(item.getId(), UserRole.ROLE_ADMIN);
            item.setToken(token);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<ItemForm> entity = new HttpEntity<>(item, headers);

            RestTemplate restTemplate = new RestTemplate();

            String backendUrl
                    = backendAddress + "item/save";

            restTemplate.exchange(
                    backendUrl,
                    HttpMethod.POST,
                    entity,
                    (Class<Object>) null);

        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);
            throw new TokenAuthorizationException("Authorization token not valid");
        }
    }

    public void deleteItem(ItemForm item) throws TokenAuthorizationException {

        try {

            Token token = authorizationService.getTokenObject(item.getId(), UserRole.ROLE_ADMIN);
            item.setToken(token);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<ItemForm> entity = new HttpEntity<>(item, headers);

            RestTemplate restTemplate = new RestTemplate();

            String backendUrl
                    = backendAddress + "item/delete";

            restTemplate.exchange(
                    backendUrl,
                    HttpMethod.POST,
                    entity,
                    (Class<Object>) null);

        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);
            throw new TokenAuthorizationException("Authorization token not valid");
        }
    }

    public void buyItem(ItemForm item, Long userId) throws ItemNotFoundException, ItemAmountNotEnoughException, TokenAuthorizationException {

        try {

            Token token = authorizationService.getTokenObject(item.getId(), UserRole.ROLE_USER);
            item.setToken(token);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<ItemForm> entity = new HttpEntity<>(item, headers);

            RestTemplate restTemplate = new RestTemplate();

            String backendUrl
                    = backendAddress + "item/buy";

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                    .queryParam("userId", userId);

            restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.POST,
                    entity,
                    (Class<Object>) null);

        } catch (HttpClientErrorException e) {

            if (e.getStatusCode() == HttpStatus.BAD_REQUEST) {
                throw new ItemAmountNotEnoughException("Amount in shop: " + getItemById(item.getId()).getAmount() + ", not enough amount of item");

            } else {
                throw new TokenAuthorizationException("Authorization token not valid");
            }
        }
    }

    public Map<String, List<String>> getErrorsForItem(BindingResult bindingResult, boolean sameName) {

        Map<String, List<String>> errorsMap = new HashMap<>();

        for (ObjectError error : bindingResult.getAllErrors()) {

            if (errorsMap.containsKey(((FieldError) error).getField())) {
                List<String> errors = errorsMap.get(((FieldError) error).getField());
                errors.add(error.getDefaultMessage());

                errorsMap.put(((FieldError) error).getField(), errors);
            } else {

                List<String> errors = new ArrayList<>();

                errors.add(error.getDefaultMessage());
                errorsMap.put(((FieldError) error).getField(), errors);
            }

            List<String> errors = new ArrayList<>();

            if (((FieldError) error).getField().equals("price")) {

                errors.add("must be only numbers with maximum 2 digits after decimal point");

                errorsMap.put(((FieldError) error).getField(), errors);
            }

            if (((FieldError) error).getField().equals("amount")) {
                errors.clear();
                errors.add("must be only numbers");

                errorsMap.put("amount", errors);
            }

        }

        if (sameName) {
            List<String> errors = new ArrayList<>();

            errors.add("item with this name exists");
            errorsMap.put("name", errors);
        }

        return errorsMap;
    }

    public Map<String, List<String>> getErrorExceptionMessage(String message) {

        Map<String, List<String>> errorsMap = new HashMap<>();

        List<String> errorList = new ArrayList<>();

        String[] errorStr = message.split(",");

        Collections.addAll(errorList, errorStr);
        Collections.reverse(errorList);

        errorsMap.put("exception", errorList);

        return errorsMap;
    }

    public ItemForm getItemByName(String name) throws ItemNotFoundException {
        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "item/getByName";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("name", name);

        try {
            ResponseEntity<ItemForm> response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<ItemForm>() {
                    });

            return response.getBody();
        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);
            throw new ItemNotFoundException("Item not found name: " + name);
        }
    }

    public boolean checkDifferentNames(ItemChangeForm itemChangeForm) throws ItemNotFoundException {
        ItemForm itemByName = getItemByName(itemChangeForm.getName());
        Long itemById = itemChangeForm.getId();

        return !itemByName.getId().equals(itemById);
    }

    public boolean checkDeletePossible(String name) {

        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "itemInOrder/checkAllWithName";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("name", name);

        ResponseEntity<Boolean> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                null,
                Boolean.class);

        if (response.getBody() != null) {
            return response.getBody();
        } else {
            return false;
        }
    }
}
