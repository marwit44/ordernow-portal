package pl.marwit44.orderNowportal.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.marwit44.orderNowportal.dto.ItemInOrder;
import pl.marwit44.orderNowportal.dto.Token;
import pl.marwit44.orderNowportal.exceptions.ItemNotFoundException;
import pl.marwit44.orderNowportal.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowportal.model.UserRole;

import java.io.UnsupportedEncodingException;
import java.util.List;

@Service
public class ItemInOrderService {

    private final AuthorizationService authorizationService;
    private final CookieModelService cookieModelService;

    private final Logger log = LoggerFactory.getLogger(ItemInOrderService.class);

    @Value("${application.backendAddress}")
    private String backendAddress;

    public ItemInOrderService(AuthorizationService authorizationService, CookieModelService cookieModelService) {
        this.authorizationService = authorizationService;
        this.cookieModelService = cookieModelService;
    }

    public void getListItemInOrderByIdWithPaginationAndFiltering(Model model, Long id, Integer pageId, String searchTerm, String sortBy, boolean desc,
                                                                 int rangeFrom, int rangeTo, boolean changeFrom, boolean changeTo) throws UnsupportedEncodingException {

        int pageNumber = ((pageId != null && pageId > 0) ? pageId : 1);

        List<ItemInOrder> itemInOrderList;

        if (rangeFrom > rangeTo && rangeFrom > -1 && rangeTo > -1) {
            int rangeTmp = rangeFrom;
            rangeFrom = rangeTo;
            rangeTo = rangeTmp;
        }

        if (searchTerm != null && !searchTerm.equals("")) {
            itemInOrderList = itemInOrderListFiltering(id, pageNumber, searchTerm, sortBy, desc, rangeFrom, rangeTo);
        } else {
            itemInOrderList = getItemInOrderList(id, pageNumber, sortBy, desc, rangeFrom, rangeTo);
        }

        int totalPages = 1;

        if (itemInOrderList.size() > 0) {
            totalPages = itemInOrderList.get(0).getTotalPages();
        }

        cookieModelService.setBasicModelParameters(model, searchTerm, sortBy, desc, rangeFrom, rangeTo, changeFrom, changeTo, pageNumber, totalPages);

        model.addAttribute("orderId", id);
        model.addAttribute("itemInOrderList", itemInOrderList);
    }

    private List<ItemInOrder> getItemInOrderList(Long id, int page, String sortBy, boolean desc, int rangeFrom, int rangeTo) {

        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "itemInOrder/getList";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("id", id)
                .queryParam("page", page)
                .queryParam("sortBy", sortBy)
                .queryParam("desc", desc)
                .queryParam("rangeFrom", rangeFrom)
                .queryParam("rangeTo", rangeTo);

        ResponseEntity<List<ItemInOrder>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ItemInOrder>>() {
                });

        return response.getBody();
    }

    private List<ItemInOrder> itemInOrderListFiltering(Long id, int page, String name, String sortBy, boolean desc, int rangeFrom, int rangeTo) {

        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "itemInOrder/filtering";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("id", id)
                .queryParam("name", name)
                .queryParam("page", page)
                .queryParam("sortBy", sortBy)
                .queryParam("desc", desc)
                .queryParam("rangeFrom", rangeFrom)
                .queryParam("rangeTo", rangeTo);

        ResponseEntity<List<ItemInOrder>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ItemInOrder>>() {
                });

        return response.getBody();
    }

    public ItemInOrder getItemById(Long id) throws ItemNotFoundException {

        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "itemInOrder/getById";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("id", id);

        try {
            ResponseEntity<ItemInOrder> response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<ItemInOrder>() {
                    });

            return response.getBody();
        } catch (HttpClientErrorException e) {
            throw new ItemNotFoundException("Item not found " + id);
        }
    }

    public void deleteItemFromOrder(ItemInOrder itemInOrder, Long orderId) throws TokenAuthorizationException {

        try {

            Token token = authorizationService.getTokenObject(itemInOrder.getId(), UserRole.ROLE_USER);
            itemInOrder.setToken(token);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<ItemInOrder> entity = new HttpEntity<>(itemInOrder, headers);

            RestTemplate restTemplate = new RestTemplate();

            String backendUrl
                    = backendAddress + "itemInOrder/delete";

            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                    .queryParam("orderId", orderId);

            restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.POST,
                    entity,
                    (Class<Object>) null);

        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);
            throw new TokenAuthorizationException("Authorization token not valid");
        }
    }
}
