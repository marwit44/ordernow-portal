package pl.marwit44.orderNowportal.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.marwit44.orderNowportal.dto.Image;
import pl.marwit44.orderNowportal.dto.ItemDetails;
import pl.marwit44.orderNowportal.dto.Token;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.exceptions.ItemDetailsNotFoundException;
import pl.marwit44.orderNowportal.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowportal.exceptions.UserNotFoundException;
import pl.marwit44.orderNowportal.form.ItemForm;
import pl.marwit44.orderNowportal.model.UserRole;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@Service
public class ItemDetailsService {

    @Autowired
    private final AuthorizationService authorizationService;
    @Autowired
    private final UserService userService;
    private final Logger log = LoggerFactory.getLogger(ItemDetailsService.class);
    @Value("${application.backendAddress}")
    private String backendAddress;

    public ItemDetailsService(AuthorizationService authorizationService, UserService userService) {
        this.authorizationService = authorizationService;
        this.userService = userService;
    }

    public ItemDetails getItemDetailsById(Long id) throws ItemDetailsNotFoundException {

        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "itemDetails/getById";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("id", id);

        try {
            ResponseEntity<ItemDetails> response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<ItemDetails>() {
                    });

            return response.getBody();
        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);

            throw new ItemDetailsNotFoundException("Item details not found id: " + id);
        }
    }

    public void getItemDetails(Model model, ItemForm item, Long id, ItemDetails itemDetails, String goBackTo, Long orderId, int pictureNumber) throws ItemDetailsNotFoundException, UserNotFoundException {

        User user = userService.getLoggedUser();

        List<Image> imageList;

        if (itemDetails == null) {
            itemDetails = getItemDetailsById(id);
            imageList = itemDetails.getImageList();
        } else {
            imageList = getItemDetailsById(id).getImageList();
        }

        int imageListSize = 0;

        if (imageList != null) {
            imageListSize = imageList.size();
        }

        if (goBackTo != null) {
            itemDetails.setGoBackTo(goBackTo);
        }

        if (orderId != null) {
            itemDetails.setOrderId(orderId);
        }

        if (imageListSize != 0) {
            model.addAttribute("imageListNotEmpty", true);
            model.addAttribute("allPictures", imageListSize);

            if (pictureNumber < 1) {
                pictureNumber = 1;
            }

            if (pictureNumber > imageListSize) {
                pictureNumber = imageListSize;
            }

            int previousPages = pictureNumber - 3;
            int nextPages = pictureNumber + 3;

            boolean showFirst = false;
            boolean showLast = false;

            if (previousPages < 1) {
                previousPages = 1;
            }

            if (nextPages > imageListSize) {
                nextPages = imageListSize;
            }

            if (pictureNumber > 1) {
                showFirst = true;
            }

            if (pictureNumber < imageListSize) {
                showLast = true;
            }

            List<Integer> pictureNumberList = IntStream.rangeClosed(previousPages, nextPages).boxed().collect(Collectors.toList());

            model.addAttribute("pictureNumber", pictureNumber);
            model.addAttribute("pictureNumberList", pictureNumberList);
            model.addAttribute("showFirst", showFirst);
            model.addAttribute("showLast", showLast);

        } else {
            model.addAttribute("imageListNotEmpty", false);
        }

        model.addAttribute("user", user);
        model.addAttribute("item", item);
        model.addAttribute("itemDetails", itemDetails);
        model.addAttribute("pictureNumber", pictureNumber);
        model.addAttribute("errorMessage", "");
    }

    public void saveDetails(ItemDetails itemDetails) throws TokenAuthorizationException {

        try {
            Token token = authorizationService.getTokenObject(itemDetails.getId(), UserRole.ROLE_ADMIN);
            itemDetails.setToken(token);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<ItemDetails> entity = new HttpEntity<>(itemDetails, headers);

            RestTemplate restTemplate = new RestTemplate();

            String backendUrl
                    = backendAddress + "itemDetails/save";

            restTemplate.exchange(
                    backendUrl,
                    HttpMethod.POST,
                    entity,
                    (Class<Object>) null);

        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);
            throw new TokenAuthorizationException("Authorization token not valid");
        }
    }

    public byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];

        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }

        try {
            outputStream.close();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        log.info("Compressed Image Byte Size - " + outputStream.toByteArray().length);

        return outputStream.toByteArray();
    }

    public byte[] decompressBytes(byte[] data) {

        Inflater inflater = new Inflater();
        inflater.setInput(data);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];

        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException | DataFormatException e) {
            log.error(e.getMessage());
        }

        return outputStream.toByteArray();
    }

    public void saveImage(Image image) throws TokenAuthorizationException {
        try {
            Token token = authorizationService.getTokenObject(image.getItemDetails().getId(), UserRole.ROLE_ADMIN);
            image.setToken(token);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<Image> entity = new HttpEntity<>(image, headers);

            RestTemplate restTemplate = new RestTemplate();

            String backendUrl
                    = backendAddress + "image/save";

            restTemplate.exchange(
                    backendUrl,
                    HttpMethod.POST,
                    entity,
                    (Class<Object>) null);

        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);
            throw new TokenAuthorizationException("Authorization token not valid");
        }
    }

    public void deleteImage(Image image) throws TokenAuthorizationException {
        try {
            Token token = authorizationService.getTokenObject(image.getId(), UserRole.ROLE_ADMIN);
            image.setToken(token);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<Image> entity = new HttpEntity<>(image, headers);

            RestTemplate restTemplate = new RestTemplate();

            String backendUrl
                    = backendAddress + "image/delete";

            restTemplate.exchange(
                    backendUrl,
                    HttpMethod.POST,
                    entity,
                    (Class<Object>) null);

        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);
            throw new TokenAuthorizationException("Authorization token not valid");
        }
    }
}
