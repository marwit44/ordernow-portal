package pl.marwit44.orderNowportal.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.marwit44.orderNowportal.Constants;
import pl.marwit44.orderNowportal.dto.Token;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.model.UserRole;
import pl.marwit44.orderNowportal.repository.UserRepository;
import pl.marwit44.orderNowportal.transformer.UserTransformer;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

@Service
public class AuthorizationService {

    private final UserRepository userRepository;

    private final Logger log = LoggerFactory.getLogger(AuthorizationService.class);

    @Autowired
    public AuthorizationService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String generateToken(Long id, LocalDateTime dateTime, UserRole role) {

        String jwtKey = "";

        if (role == UserRole.ROLE_USER) {
            jwtKey = Constants.JWT_TOKEN_KEY_USER;
        } else if (role == UserRole.ROLE_ADMIN) {
            jwtKey = Constants.JWT_TOKEN_KEY_ADMIN;
        }

        try {
            Algorithm algorithm = Algorithm.HMAC256(jwtKey);
            Date expirationDate = Date.from(ZonedDateTime.now().plusHours(24).toInstant());
            Date issuedAt = Date.from(ZonedDateTime.now().toInstant());
            return JWT.create()
                    .withIssuedAt(issuedAt)
                    .withExpiresAt(expirationDate)
                    .withClaim("id", id)
                    .withClaim("dateTime", dateTime.toString())
                    .withIssuer("jwtauth")
                    .sign(algorithm);
        } catch (UnsupportedEncodingException | JWTCreationException e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    public User validateToken(String token, UserRole role) {
        try {
            if(token != null) {

                String jwtKey = "";

                if (role == UserRole.ROLE_USER) {
                    jwtKey = Constants.JWT_TOKEN_KEY_USER;
                } else if (role == UserRole.ROLE_ADMIN) {
                    jwtKey = Constants.JWT_TOKEN_KEY_ADMIN;
                }

                Algorithm algorithm = Algorithm.HMAC256(jwtKey);
                JWTVerifier verifier = JWT.require(algorithm)
                        .withIssuer("jwtauth")
                        .build();
                DecodedJWT jwt = verifier.verify(token);

                Claim id = jwt.getClaim("id");
                Claim dateTime = jwt.getClaim("dateTime");

                LocalDateTime lastLoginDate = LocalDateTime.parse(dateTime.asString());

                return UserTransformer.getUserDtoFromModel(userRepository.getOneByIdAndLastLogin(id.asLong(),lastLoginDate));
            }
        } catch (UnsupportedEncodingException | JWTVerificationException e){
            log.error(e.getMessage(), e);
        }
        return null;
    }



    Token getTokenObject(Long id, UserRole role) {
        LocalDateTime sentRestTime = LocalDateTime.now();
        if(id == null) {
            id = 1L;
        }

        String tokenValue = generateToken(id, sentRestTime, role);

        return Token.Builder.aToken()
                .withId(id)
                .withValue(tokenValue)
                .withRole(role)
                .build();
    }
}
