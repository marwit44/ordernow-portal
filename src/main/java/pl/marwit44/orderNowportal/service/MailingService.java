package pl.marwit44.orderNowportal.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;
import pl.marwit44.orderNowportal.dto.MailDetails;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

@Service
public class MailingService {

    private final JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String emailAddress;

    @Value("${application.mail.name}")
    private String emailFromValue;

    @Autowired
    public MailingService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    void sendEmail(String to, MailDetails mailDetails, Map<String, Object> parameters) throws MessagingException, IOException, TemplateException {

        ModelAndView model = new ModelAndView("index", "template", parameters);

        String processedTemplate = processTemplate(model.getModel(), mailDetails.getTemplate());

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, "utf-8");
        mimeMessage.setContent(processedTemplate, "text/html");
        helper.setTo(to);
        helper.setSubject(mailDetails.getSubject());
        helper.setBcc(mailDetails.getBlindCopyTo());
        helper.setCc(mailDetails.getCopyTo());
        helper.setReplyTo(mailDetails.getReplyTo());
        helper.setFrom(new InternetAddress(emailAddress, emailFromValue));
        mailSender.send(mimeMessage);
    }

    private String processTemplate(Map model, String template)
            throws IOException, TemplateException {

        Configuration cfg = new Configuration(Configuration.VERSION_2_3_28);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);
        cfg.setWrapUncheckedExceptions(true);
        Template t = new Template("MailTemplate", template, cfg);
        Writer out = new StringWriter();
        t.process(model, out);
        return out.toString();
    }
}
