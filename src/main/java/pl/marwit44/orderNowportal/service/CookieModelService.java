package pl.marwit44.orderNowportal.service;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import pl.marwit44.orderNowportal.dto.CookieValues;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class CookieModelService {

    public String setOrGetCookieValue(String value, String cookieVal, String cookieName, HttpServletResponse response) throws UnsupportedEncodingException {

        Cookie cookie;

        if (value != null && !value.equals("null")) {

            cookie = new Cookie(cookieName, URLEncoder.encode(value, "UTF-8"));

            cookie.setPath("/");
            response.addCookie(cookie);

        } else {
            value = URLDecoder.decode(cookieVal, "UTF-8");
        }

        return value;
    }

    public CookieValues setBasicCookieValues(String searchTerm, String searchTermCookieVal, String searchTermCookieName, String pageIdStr, String pageIdCookieVal,
                                             String pageIdCookieName, String sortBy, String sortByCookieVal, String sortByCookieName, String desc, String descCookieVal,
                                             String descCookieName, String rangeFrom, String rangeFromCookieVal, String rangeFromCookieName,
                                             String rangeTo, String rangeToCookieVal, String rangeToCookieName, HttpServletResponse response) throws UnsupportedEncodingException {

        searchTerm = setOrGetCookieValue(searchTerm, searchTermCookieVal, searchTermCookieName, response);
        int pageId = Integer.parseInt(setOrGetCookieValue(pageIdStr, pageIdCookieVal, pageIdCookieName, response));
        sortBy = setOrGetCookieValue(sortBy, sortByCookieVal, sortByCookieName, response);
        desc = setOrGetCookieValue(desc, descCookieVal, descCookieName, response);
        rangeFrom = setOrGetCookieValue(rangeFrom, rangeFromCookieVal, rangeFromCookieName, response);
        rangeTo = setOrGetCookieValue(rangeTo, rangeToCookieVal, rangeToCookieName, response);

        if (sortBy.equals("name") || sortBy.equals("status")) {
            rangeFrom = setOrGetCookieValue("", rangeFromCookieVal, rangeFromCookieName, response);
            rangeTo = setOrGetCookieValue("", rangeToCookieVal, rangeToCookieName, response);
        }

        boolean changeFrom = true;
        boolean changeTo = true;

        if (rangeFrom.equals("")) {
            rangeFrom += -1;
        } else {
            changeFrom = false;
        }

        if (rangeTo.equals("")) {
            rangeTo += -1;
        } else {
            changeTo = false;
        }

        return CookieValues.Builder.aCookieValues()
                .withPageId(pageId)
                .withSearchTerm(searchTerm)
                .withSortBy(sortBy)
                .withDesc(Boolean.parseBoolean(desc))
                .withRangeFrom(rangeFrom)
                .withRangeTo(rangeTo)
                .withChangeFrom(changeFrom)
                .withChangeTo(changeTo)
                .build();
    }

    public void setBasicExceptionState(Model model, CookieValues cookieValues, Throwable e) {
        model.addAttribute("exception", e.getMessage());
        if (!cookieValues.getRangeFrom().equals("-1") || !cookieValues.isChangeFrom()) {
            model.addAttribute("rangeFrom", cookieValues.getRangeFrom());
        }
        if (!cookieValues.getRangeTo().equals("-1") || !cookieValues.isChangeTo()) {
            model.addAttribute("rangeTo", cookieValues.getRangeTo());
        }
    }

    public void setBasicModelParameters(Model model, String searchTerm, String sortBy, boolean desc, int rangeFrom, int rangeTo, boolean changeFrom, boolean changeTo, int pageNumber, int totalPages) throws UnsupportedEncodingException {
        if (totalPages < pageNumber) {
            pageNumber = totalPages;
        }

        if (totalPages > 0) {

            int previousPages = pageNumber - 3;
            int nextPages = pageNumber + 3;

            boolean showFirst = false;
            boolean showLast = false;

            if (previousPages < 1) {
                previousPages = 1;
            }

            if (nextPages > totalPages) {
                nextPages = totalPages;
            }

            if (pageNumber > 1) {
                showFirst = true;
            }

            if (pageNumber < totalPages) {
                showLast = true;
            }

            List<Integer> pages = IntStream.rangeClosed(previousPages, nextPages).boxed().collect(Collectors.toList());

            model.addAttribute("pageNumber", pages);
            model.addAttribute("showFirst", showFirst);
            model.addAttribute("showLast", showLast);
        } else {
            List<Integer> pages = Collections.singletonList(1);
            model.addAttribute("pageNumber", pages);
            model.addAttribute("showFirst", false);
            model.addAttribute("showLast", false);
        }

        model.addAttribute("actualPage", pageNumber);
        model.addAttribute("totalPages", totalPages);
        if (searchTerm != null) {
            model.addAttribute("searchTerm", URLDecoder.decode(searchTerm, "UTF-8"));
        } else {
            model.addAttribute("searchTerm", "");
        }

        if (sortBy != null) {
            model.addAttribute("sortBy", URLDecoder.decode(sortBy, "UTF-8"));
        } else {
            model.addAttribute("sortBy", "name");
        }

        model.addAttribute("desc", desc);

        if (rangeTo != -1 || !changeTo) {
            model.addAttribute("rangeTo", rangeTo);
        } else {
            model.addAttribute("rangeTo ", "");
        }

        if (rangeFrom != -1 || !changeFrom) {
            model.addAttribute("rangeFrom", rangeFrom);
        } else {
            model.addAttribute("rangeFrom ", "");
        }

        if (rangeTo < 0 && !changeTo) {
            model.addAttribute("exception", "Range To is lower than 0");
        }

        if (rangeFrom < 0 && !changeFrom) {
            model.addAttribute("exception", "Range From is lower than 0");
        }

        if (rangeFrom < 0 && !changeFrom && rangeTo < 0 && !changeTo) {
            model.addAttribute("exception", "Range From and To is lower than 0");
        }
    }


    public CookieValues setItemDetailsCookies(String pictureNumberStr, String pictureNumberCookieVal, String pictureNumberCookieName, String goBackTo, String goBackToCookieVal, String goBackToCookieName, String orderIdStr, String orderIdCookieVal, String orderIdCookieName, HttpServletResponse response) throws UnsupportedEncodingException {

        int pictureNumber = Integer.parseInt(setOrGetCookieValue(pictureNumberStr, pictureNumberCookieVal, pictureNumberCookieName, response));
        goBackTo = setOrGetCookieValue(goBackTo, goBackToCookieVal, goBackToCookieName, response);
        Long orderId = Long.parseLong(setOrGetCookieValue(orderIdStr, orderIdCookieVal, orderIdCookieName, response));

        return CookieValues.Builder.aCookieValues()
                .withPictureNumber(pictureNumber)
                .withGoBackTo(goBackTo)
                .withOrderId(orderId)
                .build();
    }
}
