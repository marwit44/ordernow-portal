package pl.marwit44.orderNowportal.service;

import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import pl.marwit44.orderNowportal.dto.Item;
import pl.marwit44.orderNowportal.dto.MailDetails;
import pl.marwit44.orderNowportal.dto.Order;
import pl.marwit44.orderNowportal.dto.Token;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.exceptions.OrderNotFoundException;
import pl.marwit44.orderNowportal.exceptions.SendRestartEmailException;
import pl.marwit44.orderNowportal.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowportal.model.MailTypes;
import pl.marwit44.orderNowportal.model.OrderStatus;
import pl.marwit44.orderNowportal.model.UserRole;

import javax.mail.MessagingException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderService {

    @Autowired
    private final AuthorizationService authorizationService;
    @Autowired
    private final UserService userService;
    private final CookieModelService cookieModelService;

    private final Logger log = LoggerFactory.getLogger(OrderService.class);
    private final MailingService mailingService;
    private final MailTemplatesService mailTemplatesService;
    @Value("${application.backendAddress}")
    private String backendAddress;

    public OrderService(AuthorizationService authorizationService, UserService userService, CookieModelService cookieModelService, MailingService mailingService, MailTemplatesService mailTemplatesService) {
        this.authorizationService = authorizationService;
        this.userService = userService;
        this.cookieModelService = cookieModelService;
        this.mailingService = mailingService;
        this.mailTemplatesService = mailTemplatesService;
    }

    public List<Order> getListOrdersWithPaginationAndFiltering(Model model, User user, Integer pageId, String searchTerm, String sortBy, boolean desc,
                                                               String rangeFrom, String rangeTo, boolean changeFrom, boolean changeTo) throws UnsupportedEncodingException {
        int pageNumber = ((pageId != null && pageId > 0) ? pageId : 1);

        List<Order> allOrders;
        long rangeFromVal = -1;
        long rangeToVal = -1;

        LocalDateTime rangeFromDateTime = LocalDateTime.now();
        LocalDateTime rangeToDateTime = LocalDateTime.now();

        model.addAttribute("rangeFromDT", LocalDateTime.now());
        model.addAttribute("rangeToDT", LocalDateTime.now());


        if (sortBy.equals("addDate")) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            if (!rangeFrom.equals("-1")) {
                rangeFromDateTime = LocalDateTime.parse(rangeFrom, formatter);
                rangeFromVal = rangeFromDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
                changeFrom = true;
            }
            if (!rangeTo.equals("-1")) {
                rangeToDateTime = LocalDateTime.parse(rangeTo, formatter);
                rangeToVal = rangeToDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
                changeTo = true;
            }
        } else {
            rangeFromVal = Long.parseLong(rangeFrom);
            rangeToVal = Long.parseLong(rangeTo);
        }

        boolean changeAddDate = false;

        if (rangeFromVal > rangeToVal && rangeFromVal > -1 && rangeToVal > -1) {
            long rangeTmpVal = rangeFromVal;
            rangeFromVal = rangeToVal;
            rangeToVal = rangeTmpVal;

            LocalDateTime rangeTmpDateTime = rangeFromDateTime;
            rangeFromDateTime = rangeToDateTime;
            rangeToDateTime = rangeTmpDateTime;

            changeAddDate = true;
        }


        if (searchTerm != null && !searchTerm.equals("")) {
            allOrders = ordersFiltering(user.getId(), user.getRole(), pageNumber, searchTerm, sortBy, desc, rangeFromVal, rangeToVal, rangeFromDateTime, rangeToDateTime);

        } else {
            allOrders = getAllOrders(user.getId(), user.getRole(), pageNumber, sortBy, desc, rangeFromVal, rangeToVal, rangeFromDateTime, rangeToDateTime);
        }

        int totalPages = 1;

        if (allOrders.size() > 0) {
            totalPages = allOrders.get(0).getTotalPages();
        }

        cookieModelService.setBasicModelParameters(model, searchTerm, sortBy, desc, (int) rangeFromVal, (int) rangeToVal, changeFrom, changeTo, pageNumber, totalPages);

        model.addAttribute("userId", user.getId());
        model.addAttribute("userRole", user.getRole());
        model.addAttribute("allOrders", allOrders);

        if (sortBy.equals("addDate")) {
            if (!rangeFrom.equals("-1")) {
                model.addAttribute("rangeFrom", rangeFrom);
            }
            if (!rangeTo.equals("-1")) {
                model.addAttribute("rangeTo", rangeTo);
            }
            if (changeAddDate) {
                model.addAttribute("rangeFrom", rangeTo);
                model.addAttribute("rangeTo", rangeFrom);
            }
        }

        return allOrders;
    }

    private List<Order> getAllOrders(Long userId, UserRole role, int page, String sortBy, boolean desc, long rangeFrom, long rangeTo, LocalDateTime rangeFromDateTime, LocalDateTime rangeToDateTime) {

        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "order/getAll";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("id", userId)
                .queryParam("role", role)
                .queryParam("page", page)
                .queryParam("sortBy", sortBy)
                .queryParam("desc", desc)
                .queryParam("rangeFrom", rangeFrom)
                .queryParam("rangeTo", rangeTo)
                .queryParam("rangeFromDateTime", rangeFromDateTime)
                .queryParam("rangeToDateTime", rangeToDateTime);

        ResponseEntity<List<Order>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Order>>() {
                });

        return response.getBody();
    }

    private List<Order> ordersFiltering(Long userId, UserRole role, int page, String name, String sortBy, boolean desc, long rangeFrom, long rangeTo, LocalDateTime rangeFromDateTime, LocalDateTime rangeToDateTime) {

        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "order/filtering";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("id", userId)
                .queryParam("role", role)
                .queryParam("name", name)
                .queryParam("page", page)
                .queryParam("sortBy", sortBy)
                .queryParam("desc", desc)
                .queryParam("rangeFrom", rangeFrom)
                .queryParam("rangeTo", rangeTo)
                .queryParam("rangeFromDateTime", rangeFromDateTime)
                .queryParam("rangeToDateTime", rangeToDateTime);

        ResponseEntity<List<Order>> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Order>>() {
                });

        return response.getBody();
    }

    public Order getNotAccepted(Long userId) {

        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "order/getNotAccepted";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("userId", userId);

        ResponseEntity<Order> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<Order>() {
                });

        return response.getBody();
    }

    public Order getOrderById(Long id) throws OrderNotFoundException {

        RestTemplate restTemplate = new RestTemplate();

        String backendUrl
                = backendAddress + "order/getById";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(backendUrl)
                .queryParam("id", id);

        try {
            ResponseEntity<Order> response = restTemplate.exchange(
                    builder.toUriString(),
                    HttpMethod.GET,
                    null,
                    new ParameterizedTypeReference<Order>() {
                    });

            return response.getBody();
        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);
            throw new OrderNotFoundException("Order not found id: " + id);
        }
    }

    public void deleteOrder(Order order) throws TokenAuthorizationException {

        try {

            Token token = authorizationService.getTokenObject(order.getId(), UserRole.ROLE_USER);
            order.setToken(token);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<Order> entity = new HttpEntity<>(order, headers);

            RestTemplate restTemplate = new RestTemplate();

            String backendUrl
                    = backendAddress + "order/delete";

            restTemplate.exchange(
                    backendUrl,
                    HttpMethod.POST,
                    entity,
                    (Class<Object>) null);

        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);
            throw new TokenAuthorizationException("Authorization token not valid");
        }
    }

    public void acceptOrder(Order order) throws TokenAuthorizationException {

        try {

            Token token = authorizationService.getTokenObject(order.getId(), UserRole.ROLE_USER);
            order.setToken(token);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<Order> entity = new HttpEntity<>(order, headers);

            RestTemplate restTemplate = new RestTemplate();

            String backendUrl
                    = backendAddress + "order/accept";

            restTemplate.exchange(
                    backendUrl,
                    HttpMethod.POST,
                    entity,
                    (Class<Object>) null);

        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);
            throw new TokenAuthorizationException("Authorization token not valid");
        }
    }

    public void editOrder(Order order, OrderStatus oldStatus) throws TokenAuthorizationException {

        try {

            Token token = authorizationService.getTokenObject(order.getId(), UserRole.ROLE_ADMIN);
            order.setToken(token);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<Order> entity = new HttpEntity<>(order, headers);

            RestTemplate restTemplate = new RestTemplate();

            String backendUrl
                    = backendAddress + "order/edit";

            restTemplate.exchange(
                    backendUrl,
                    HttpMethod.POST,
                    entity,
                    (Class<Object>) null);

            sendConfirmEmail(order, oldStatus);

        } catch (HttpClientErrorException e) {

            log.error(e.getMessage(), e);
            throw new TokenAuthorizationException("Authorization token not valid");
        } catch (MessagingException | IOException | TemplateException e) {
            throw new SendRestartEmailException("Failed to send confirm order email");
        }
    }

    public void sendConfirmEmail(Order order, OrderStatus oldStatus) throws MessagingException, IOException, TemplateException {

        MailDetails mailDetailsDto = new MailDetails();
        User user = userService.getUserById(order.getUserId());
        OrderStatus status = order.getStatus();

        Map<String, Object> orderEmailInfo = new HashMap<>();
        orderEmailInfo.put("name", user.getFirstName() + " " + user.getLastName());
        orderEmailInfo.put("price", order.getPrice());

        StringBuilder text = new StringBuilder();
        text.append("name ").append("amount ").append("price ").append("<br>");

        for (Item item : order.getItemList()) {
            text.append(item.getName()).append(" ").append(item.getAmount()).append(" ").append(item.getPrice()).append("<br>");
        }

        orderEmailInfo.put("itemList", text);

        if (oldStatus.getValue() != status.getValue()) {

            if (status.getValue() == 1) {
                mailDetailsDto = mailTemplatesService.getByType(MailTypes.SENDING);

            } else if (status.getValue() == 2) {
                mailDetailsDto = mailTemplatesService.getByType(MailTypes.READY_TO_DELIVER);

            } else if (status.getValue() == 3) {
                mailDetailsDto = mailTemplatesService.getByType(MailTypes.DONE);

            }

            mailingService.sendEmail(user.getEmail(), mailDetailsDto, orderEmailInfo);
        }
    }
}
