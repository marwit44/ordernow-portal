package pl.marwit44.orderNowportal.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import pl.marwit44.orderNowportal.dto.MailDetails;
import pl.marwit44.orderNowportal.dto.MailTemplate;
import pl.marwit44.orderNowportal.exceptions.EditMailTemplateException;
import pl.marwit44.orderNowportal.model.MailTemplateBodyModel;
import pl.marwit44.orderNowportal.model.MailTemplateModel;
import pl.marwit44.orderNowportal.model.MailTypes;
import pl.marwit44.orderNowportal.repository.MailTemplateBodyRepository;
import pl.marwit44.orderNowportal.repository.MailTemplatesRepository;
import pl.marwit44.orderNowportal.specifications.EmailSpecification;
import pl.marwit44.orderNowportal.transformer.MailTemplatesTransformer;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class MailTemplatesService {

    private final MailTemplatesRepository mailTemplatesRepository;
    private final MailTemplateBodyRepository mailTemplateBodyRepository;

    private Logger log = LoggerFactory.getLogger(MailTemplatesService.class);

    @Value("${spring.mail.username}")
    private String emailAddress;

    @Autowired
    public MailTemplatesService(MailTemplatesRepository mailTemplatesRepository, MailTemplateBodyRepository mailTemplateBodyRepository) {
        this.mailTemplatesRepository = mailTemplatesRepository;
        this.mailTemplateBodyRepository = mailTemplateBodyRepository;
    }


    public void getListItemWithPaginationAndFiltering(Model model, Integer pageId, String searchTerm) throws UnsupportedEncodingException {
        int pageNumber = ((pageId != null && pageId > 0) ? pageId : 1);

        List<MailTemplate> allTemplates;

        if (searchTerm != null && !searchTerm.equals("")) {
            allTemplates = templatesFiltering(pageNumber, searchTerm);
        } else {
            allTemplates = getAllTemplates(pageNumber);
        }

        int totalPages = 1;

        if (allTemplates.size() > 0) {
            totalPages = allTemplates.get(0).getTotalPages();
        }

        if (totalPages < pageNumber) {
            pageNumber = totalPages;
        }

        if (totalPages > 0) {

            int previousPages = pageNumber - 3;
            int nextPages = pageNumber + 3;

            if (previousPages < 1) {
                previousPages = 1;
            }

            if (nextPages > totalPages) {
                nextPages = totalPages;
            }

            List<Integer> pages = IntStream.rangeClosed(previousPages, nextPages).boxed().collect(Collectors.toList());

            model.addAttribute("pageNumber", pages);
        } else {
            List<Integer> pages = Collections.singletonList(1);
            model.addAttribute("pageNumber", pages);
        }

        model.addAttribute("actualPage", pageNumber);
        model.addAttribute("emails", allTemplates);
        if (searchTerm != null) {
            model.addAttribute("searchTerm", URLDecoder.decode(searchTerm, "UTF-8"));
        } else {
            model.addAttribute("searchTerm", "");
        }
    }

    List<MailTemplate> getAllTemplates(int pageNumber) {

        try {

            long allTemplatesSizePages = (mailTemplatesRepository.count() - 1) / 10 + 1;

            if (pageNumber > (int) allTemplatesSizePages) {
                pageNumber = (int) allTemplatesSizePages;
            }

            if (pageNumber <= 0) {
                pageNumber = 1;
            }

            PageRequest pageable = PageRequest.of(pageNumber - 1, 10, Sort.by("name"));

            Page<MailTemplateModel> all = mailTemplatesRepository.findAll(pageable);

            List<MailTemplate> templateList = all.getContent().stream()
                    .map(MailTemplatesTransformer::getMailTemplateDto)
                    .collect(Collectors.toList());

            if (templateList.size() > 0) {
                templateList.get(0).setTotalPages(all.getTotalPages());
            }

            return templateList;

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ArrayList<>();
        }
    }

    List<MailTemplate> templatesFiltering(int pageNumber, String searchTerm) {

        EmailSpecification emailSpecification = new EmailSpecification(searchTerm);

        long allTemplatesSizePages = (mailTemplatesRepository.count(emailSpecification) - 1) / 10 + 1;

        if (pageNumber > (int) allTemplatesSizePages) {
            pageNumber = (int) allTemplatesSizePages;
        }

        if (pageNumber <= 0) {
            pageNumber = 1;
        }

        PageRequest pageable = PageRequest.of(pageNumber - 1, 10, Sort.by("name"));

        Page<MailTemplateModel> all = mailTemplatesRepository.findAll(emailSpecification, pageable);

        List<MailTemplate> templateList = all.getContent().stream()
                .map(MailTemplatesTransformer::getMailTemplateDto)
                .collect(Collectors.toList());

        if (templateList.size() > 0) {
            templateList.get(0).setTotalPages(all.getTotalPages());
        }

        return templateList;
    }

    public MailDetails getMailTemplateDetail(Long id) {
        Optional<MailTemplateBodyModel> optionalMailDetails = mailTemplateBodyRepository.findById(id);
        MailDetails mailDetails;
        if (optionalMailDetails.isPresent()) {
            mailDetails = MailTemplatesTransformer.getMailDetailsDto(optionalMailDetails.get());
            mailDetails.setFrom(emailAddress);
        } else {
            mailDetails = new MailDetails();
        }
        return mailDetails;
    }

    public void editMailTemplate(MailDetails mailDetails) throws EditMailTemplateException {

        Optional<MailTemplateBodyModel> mailTemplateBodyOptional = mailTemplateBodyRepository.findById(mailDetails.getId());
        if (mailTemplateBodyOptional.isPresent()) {
            MailTemplateBodyModel mailTemplateBody = mailTemplateBodyOptional.get();
            MailTemplateModel mailTemplateModel = mailTemplateBody.getMail();

            mailTemplateBody.setTemplate(mailDetails.getTemplate());

            mailTemplateModel.setBlindCopyTo(mailDetails.getBlindCopyTo());
            mailTemplateModel.setCopyTo(mailDetails.getCopyTo());
            mailTemplateModel.setLastModifyDate(LocalDateTime.now());
            mailTemplateModel.setReplyTo(mailDetails.getReplyTo());
            mailTemplateModel.setSubject(mailDetails.getSubject());

            mailTemplatesRepository.save(mailTemplateModel);
        } else {
            throw new EditMailTemplateException("Could not save data, try next time");
        }
    }

    MailDetails getByType(MailTypes type) {
        return MailTemplatesTransformer.getMailDetailsDto(mailTemplatesRepository.findByType(type).getMailTemplateBodyModel());
    }
}
