package pl.marwit44.orderNowportal.dto;

public class CookieValues {

    private int pageId;
    private String searchTerm;
    private String sortBy;
    private boolean desc;
    private String rangeFrom;
    private String rangeTo;
    private boolean changeFrom;
    private boolean changeTo;
    private int pictureNumber;
    private String goBackTo;
    private Long orderId;

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public boolean isDesc() {
        return desc;
    }

    public void setDesc(boolean desc) {
        this.desc = desc;
    }

    public String getRangeFrom() {
        return rangeFrom;
    }

    public void setRangeFrom(String rangeFrom) {
        this.rangeFrom = rangeFrom;
    }

    public String getRangeTo() {
        return rangeTo;
    }

    public void setRangeTo(String rangeTo) {
        this.rangeTo = rangeTo;
    }

    public boolean isChangeFrom() {
        return changeFrom;
    }

    public void setChangeFrom(boolean changeFrom) {
        this.changeFrom = changeFrom;
    }

    public boolean isChangeTo() {
        return changeTo;
    }

    public void setChangeTo(boolean changeTo) {
        this.changeTo = changeTo;
    }

    public int getPictureNumber() {
        return pictureNumber;
    }

    public void setPictureNumber(int pictureNumber) {
        this.pictureNumber = pictureNumber;
    }

    public String getGoBackTo() {
        return goBackTo;
    }

    public void setGoBackTo(String goBackTo) {
        this.goBackTo = goBackTo;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public static final class Builder {
        private int pageId;
        private String searchTerm;
        private String sortBy;
        private boolean desc;
        private String rangeFrom;
        private String rangeTo;
        private boolean changeFrom;
        private boolean changeTo;
        private int pictureNumber;
        private String goBackTo;
        private Long orderId;

        private Builder() {
        }

        public static Builder aCookieValues() {
            return new Builder();
        }

        public Builder withPageId(int pageId) {
            this.pageId = pageId;
            return this;
        }

        public Builder withSearchTerm(String searchTerm) {
            this.searchTerm = searchTerm;
            return this;
        }

        public Builder withSortBy(String sortBy) {
            this.sortBy = sortBy;
            return this;
        }

        public Builder withDesc(boolean desc) {
            this.desc = desc;
            return this;
        }

        public Builder withRangeFrom(String rangeFrom) {
            this.rangeFrom = rangeFrom;
            return this;
        }

        public Builder withRangeTo(String rangeTo) {
            this.rangeTo = rangeTo;
            return this;
        }

        public Builder withChangeFrom(boolean changeFrom) {
            this.changeFrom = changeFrom;
            return this;
        }

        public Builder withChangeTo(boolean changeTo) {
            this.changeTo = changeTo;
            return this;
        }

        public Builder withPictureNumber(int pictureNumber) {
            this.pictureNumber = pictureNumber;
            return this;
        }

        public Builder withGoBackTo(String goBackTo) {
            this.goBackTo = goBackTo;
            return this;
        }

        public Builder withOrderId(Long orderId) {
            this.orderId = orderId;
            return this;
        }

        public CookieValues build() {
            CookieValues cookieValues = new CookieValues();
            cookieValues.setPageId(pageId);
            cookieValues.setSearchTerm(searchTerm);
            cookieValues.setSortBy(sortBy);
            cookieValues.setDesc(desc);
            cookieValues.setRangeFrom(rangeFrom);
            cookieValues.setRangeTo(rangeTo);
            cookieValues.setChangeFrom(changeFrom);
            cookieValues.setChangeTo(changeTo);
            cookieValues.setPictureNumber(pictureNumber);
            cookieValues.setGoBackTo(goBackTo);
            cookieValues.setOrderId(orderId);
            return cookieValues;
        }
    }
}
