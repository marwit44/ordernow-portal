package pl.marwit44.orderNowportal.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

public class ItemDetails {

    private Long id;

    @NotBlank
    @Size(max = 3500)
    private String description;

    private Item item;

    private List<Image> imageList;

    private Token token;

    private String goBackTo = "items";

    private Long orderId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public List<Image> getImageList() {
        return imageList;
    }

    public void setImageList(List<Image> imageList) {
        this.imageList = imageList;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public String getGoBackTo() {
        return goBackTo;
    }

    public void setGoBackTo(String goBackTo) {
        this.goBackTo = goBackTo;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public static final class Builder {
        private Long id;
        private String description;
        private Item item;
        private List<Image> imageList;
        private Token token;
        private String goBackTo = "items";
        private Long orderId;

        private Builder() {
        }

        public static Builder anItemDetails() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withItem(Item item) {
            this.item = item;
            return this;
        }

        public Builder withImageList(List<Image> imageList) {
            this.imageList = imageList;
            return this;
        }

        public Builder withToken(Token token) {
            this.token = token;
            return this;
        }

        public Builder withGoBackTo(String goBackTo) {
            this.goBackTo = goBackTo;
            return this;
        }

        public Builder withOrderId(Long orderId) {
            this.orderId = orderId;
            return this;
        }

        public ItemDetails build() {
            ItemDetails itemDetails = new ItemDetails();
            itemDetails.setId(id);
            itemDetails.setDescription(description);
            itemDetails.setItem(item);
            itemDetails.setImageList(imageList);
            itemDetails.setToken(token);
            itemDetails.setGoBackTo(goBackTo);
            itemDetails.setOrderId(orderId);
            return itemDetails;
        }
    }
}
