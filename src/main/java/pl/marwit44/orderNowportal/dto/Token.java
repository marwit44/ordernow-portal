package pl.marwit44.orderNowportal.dto;

import pl.marwit44.orderNowportal.model.UserRole;

public class Token {

    private Long id;

    private String value;

    private UserRole role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }


    public static final class Builder {
        private Long id;
        private String value;
        private UserRole role;

        private Builder() {
        }

        public static Builder aToken() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withValue(String value) {
            this.value = value;
            return this;
        }

        public Builder withRole(UserRole role) {
            this.role = role;
            return this;
        }

        public Token build() {
            Token token = new Token();
            token.setId(id);
            token.setValue(value);
            token.setRole(role);
            return token;
        }
    }
}
