package pl.marwit44.orderNowportal.dto;

import pl.marwit44.orderNowportal.model.MailTypes;

import java.time.LocalDateTime;

public class MailTemplate {

    private Long id;

    private String name;

    private MailTypes mailType;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Long version = 0L;

    private int totalPages;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MailTypes getMailType() {
        return mailType;
    }

    public void setMailType(MailTypes mailType) {
        this.mailType = mailType;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}
