package pl.marwit44.orderNowportal.dto;

import pl.marwit44.orderNowportal.model.TokenTypes;

import java.time.LocalDateTime;

public class Verification {

    private Long id;

    private String token;

    private LocalDateTime expiryDate;

    private TokenTypes type;

    private User user;

    private LocalDateTime creationDate;

    private LocalDateTime updateDate;

    private Long version = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public TokenTypes getType() {
        return type;
    }

    public void setType(TokenTypes type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }


    public static final class Builder {
        private String token;
        private LocalDateTime expiryDate;
        private TokenTypes type;

        private Builder() {
        }

        public static Builder aVerification() {
            return new Builder();
        }

        public Builder withToken(String token) {
            this.token = token;
            return this;
        }

        public Builder withExpiryDate(LocalDateTime expiryDate) {
            this.expiryDate = expiryDate;
            return this;
        }

        public Builder withType(TokenTypes type) {
            this.type = type;
            return this;
        }

        public Verification build() {
            Verification verification = new Verification();
            verification.setToken(token);
            verification.setExpiryDate(expiryDate);
            verification.setType(type);
            Long version = 0L;
            verification.setVersion(version);
            return verification;
        }
    }
}
