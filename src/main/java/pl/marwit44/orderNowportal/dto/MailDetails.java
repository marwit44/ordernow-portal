package pl.marwit44.orderNowportal.dto;

import pl.marwit44.orderNowportal.model.MailTypes;

public class MailDetails {

    private Long id;

    private String name;

    private String template;

    private String copyTo;

    private String blindCopyTo;

    private String replyTo;

    private String subject;

    private MailTypes type;

    private String from;

    private String searchTerm;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getCopyTo() {
        return copyTo;
    }

    public void setCopyTo(String copyTo) {
        this.copyTo = copyTo;
    }

    public String getBlindCopyTo() {
        return blindCopyTo;
    }

    public void setBlindCopyTo(String blindCopyTo) {
        this.blindCopyTo = blindCopyTo;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public MailTypes getType() {
        return type;
    }

    public void setType(MailTypes type) {
        this.type = type;
    }

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }
}
