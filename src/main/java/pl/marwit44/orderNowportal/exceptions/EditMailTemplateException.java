package pl.marwit44.orderNowportal.exceptions;

public class EditMailTemplateException extends RuntimeException{
    public EditMailTemplateException(String message) {
        super(message);
    }
}
