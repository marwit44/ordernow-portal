package pl.marwit44.orderNowportal.exceptions;

public class ItemNotFoundException extends Throwable {
    public ItemNotFoundException(String message) {
        super(message);
    }
}
