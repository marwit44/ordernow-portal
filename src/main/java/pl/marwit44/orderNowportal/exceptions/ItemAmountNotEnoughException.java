package pl.marwit44.orderNowportal.exceptions;

public class ItemAmountNotEnoughException extends Throwable {
    public ItemAmountNotEnoughException(String message) {
        super(message);
    }
}
