package pl.marwit44.orderNowportal.exceptions;

public class ItemDetailsNotFoundException extends Throwable {
    public ItemDetailsNotFoundException(String message) {
        super(message);
    }
}
