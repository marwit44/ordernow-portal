package pl.marwit44.orderNowportal.exceptions;

public class TokenNotExistException extends RuntimeException {
    public TokenNotExistException(String message) {
        super(message);
    }
}
