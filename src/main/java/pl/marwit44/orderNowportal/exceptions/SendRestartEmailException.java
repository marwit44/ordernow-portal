package pl.marwit44.orderNowportal.exceptions;

public class SendRestartEmailException extends RuntimeException {
    public SendRestartEmailException(String message) {
        super(message);
    }
}
