package pl.marwit44.orderNowportal.exceptions;

public class TokenAuthorizationException extends Throwable {
    public TokenAuthorizationException(String message) {
        super(message);
    }
}
