package pl.marwit44.orderNowportal.controller;

import freemarker.template.TemplateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;
import pl.marwit44.orderNowportal.dto.Address;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.exceptions.SendRestartEmailException;
import pl.marwit44.orderNowportal.exceptions.TokenNotExistException;
import pl.marwit44.orderNowportal.exceptions.UserNotFoundException;
import pl.marwit44.orderNowportal.form.EmailForm;
import pl.marwit44.orderNowportal.form.PasswordForm;
import pl.marwit44.orderNowportal.model.TokenTypes;
import pl.marwit44.orderNowportal.service.UserService;
import pl.marwit44.orderNowportal.validation.PasswordValidator;

import javax.mail.MessagingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Controller
@RequestMapping("/users")
public class UserController {

    private final PasswordValidator passwordValidator;
    private final UserService userService;

    private final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    public UserController(UserService userService, PasswordValidator passwordValidator) {
        this.userService = userService;
        this.passwordValidator = passwordValidator;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping({"/list/page/{pageId}", "/list/page/"})
    public String getAll(Model model, @PathVariable(required = false) Integer pageId, @RequestParam(required = false) String searchTerm, HttpServletResponse response) throws UnsupportedEncodingException {

        Cookie cookie;
        if (searchTerm != null) {
            cookie = new Cookie("searchTermCookie", URLEncoder.encode(searchTerm, "UTF-8"));
        } else {
            cookie = new Cookie("searchTermCookie", URLEncoder.encode("", "UTF-8"));
        }
        cookie.setPath("/");
        response.addCookie(cookie);

        userService.getListItemWithPaginationAndFiltering(model, pageId, searchTerm);

        return "users";
    }

    @GetMapping("/profile")
    public String getProfile(Model model) {
        try {
            User user = userService.getLoggedUser();

            user = userService.getUserProfile(user.getId());

            model.addAttribute("user", user);
            model.addAttribute("address", user.getAddress());
            model.addAttribute("disableSave", false);

            return "user_profile";
        } catch (UserNotFoundException e) {
            User user = new User();
            Address address = new Address();

            model.addAttribute("user", user);
            model.addAttribute("address", address);
            model.addAttribute("exception", e.getMessage());
            model.addAttribute("disableSave", true);

            return "user_profile";
        }
    }

    @PostMapping("/edit")
    public String editUser(@ModelAttribute @Valid Address address, BindingResult bindingResult, Model model, RedirectAttributesModelMap modelRedirect) {

        if (!bindingResult.hasErrors()) {

            User user = userService.getUserProfile(address.getId());

            userService.saveAddress(user, address);

            modelRedirect.addFlashAttribute("successText", "Profile save success");

            return "redirect:/users/profile";
        } else {
            User user = userService.getUserProfile(address.getId());

            model.addAttribute("user", user);
            model.addAttribute("disableSave", false);

            return "user_profile";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/details")
    public String getUserDetails(@RequestParam(name = "id") Long id, Model model) {

        User user = userService.getUserProfile(id);

        model.addAttribute("user", user);
        model.addAttribute("address", user.getAddress());

        return "user_details_modal";
    }

    @GetMapping("/add")
    public String getAddUserForm(Model model) {

        User user = new User();
        Address address = new Address();

        model.addAttribute("user", user);
        model.addAttribute("address", address);

        return "user_add";
    }

    @PostMapping("/add")
    public String createUser(@ModelAttribute @Valid User user, BindingResult userBindingResult, @ModelAttribute @Valid Address address, BindingResult addressBindingResult, Model model) {

        if (userBindingResult.hasErrors() || addressBindingResult.hasErrors()) {
            model.addAttribute("user", user);
            model.addAttribute("address", address);

            return "user_add";
        }

        try {
            User createdUser = userService.createUser(user, address);

            if (createdUser != null) {
                model.addAttribute("createdUser", createdUser);
                return "registration_successful";
            } else {
                model.addAttribute("emailSendFailure");
                return "registration_failure";
            }

        } catch (MessagingException | IOException | TemplateException e) {

            log.error(e.getMessage(), e);
            model.addAttribute("emailSendFailure");
            return "registration_failure";
        }
    }

    @GetMapping("activate/{token}")
    public String activateUserByToken(@PathVariable String token, Model model) {

        if (userService.isTokenCorrect(token, TokenTypes.ACTIVATION)) {

            try {
                model.addAttribute("user", userService.getUserFromToken(token));
                model.addAttribute("token", token);
                model.addAttribute("passwordForm", new PasswordForm());

            } catch (UserNotFoundException e) {

                log.error(e.getMessage(), e);
                model.addAttribute("error", e.getMessage());
                return "token_error";
            }

            return "user_activation";
        } else {
            model.addAttribute("error", "Token does not exist or has been already used.");

            return "token_error";
        }
    }

    @PostMapping("activate/{token}")
    public String setUserPassword(@PathVariable String token, @Valid PasswordForm passwordForm, BindingResult bindingResult, Model model) {
        passwordValidator.validate(passwordForm, bindingResult);

        User user;

        try {
            user = userService.getUserFromToken(token);

            if (bindingResult.hasErrors()) {

                model.addAttribute("token", token);
                model.addAttribute("user", user);
                model.addAttribute("passwordForm", passwordForm);

                return "user_activation";
            }
        } catch (UserNotFoundException e) {

            log.error(e.getMessage(), e);
            model.addAttribute("error", e.getMessage());
            return "token_error";
        }

        try {
            userService.activateUser(token, passwordForm.getPassword());
        } catch (TokenNotExistException e) {
            model.addAttribute("error", e.getMessage());
            return "token_error";
        }

        if (user != null) {

            if(user.getVerification().getType() == TokenTypes.ACTIVATION) {
                return "thanks_activation";
            } else if(user.getVerification().getType() == TokenTypes.RESTART) {
                return "reset_password_success";
            }
        }

        model.addAttribute("error", "Token does not exist");
        return "token_error";
    }

    @GetMapping("/restart_password")
    public String restartPassword(Model model) {
        EmailForm email = new EmailForm();
        email.setEmail("");
        model.addAttribute("emailForm", new EmailForm());
        return "restart_password";
    }

    @PostMapping("/restart_password")
    public String sendRestartEmail(@Valid EmailForm emailForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("emailForm", emailForm);
            return "restart_password";
        }
        try {
            userService.sendRestartEmail(emailForm.getEmail());
            return "sent_email_to_restart";
        } catch (SendRestartEmailException e) {
            model.addAttribute("exception", e.getMessage());
            model.addAttribute("emailForm", emailForm);
            return "restart_password";
        }

    }

    @GetMapping("/restart_password/{token}")
    public String restartPasswordByToken(@PathVariable String token, Model model) {

        if (userService.isTokenCorrect(token, TokenTypes.RESTART)) {

            try {
                model.addAttribute("user", userService.getUserFromToken(token));
                model.addAttribute("token", token);
                model.addAttribute("passwordForm", new PasswordForm());

            } catch (UserNotFoundException e) {

                log.error(e.getMessage(), e);
                model.addAttribute("error", e.getMessage());
                return "token_error";
            }

            return "user_activation";
        }
        model.addAttribute("error", "Token does not exist or has been already used.");
        return "token_error";
    }
}
