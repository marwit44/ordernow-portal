package pl.marwit44.orderNowportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import pl.marwit44.orderNowportal.dto.Order;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.exceptions.UserNotFoundException;
import pl.marwit44.orderNowportal.model.UserRole;
import pl.marwit44.orderNowportal.service.OrderService;
import pl.marwit44.orderNowportal.service.UserService;

@ControllerAdvice
public class AdviceController {

    private final OrderService orderService;
    private final UserService userService;

    @Autowired
    public AdviceController(OrderService orderService, UserService userService) {
        this.orderService = orderService;
        this.userService = userService;
    }

    @ModelAttribute("shoppingCardPrice")
    Double getOrderPrice() {
        try {

            User user = userService.getLoggedUser();
            Order order = orderService.getNotAccepted(user.getId());

            if(order == null) {
                return 0.00;
            }
            return order.getPrice();

        } catch (UserNotFoundException e) {

            return 0.00;
        }
    }

    @ModelAttribute("userRole")
    UserRole getUserData() {
        try {

            User user = userService.getLoggedUser();

            return user.getRole();

        } catch (UserNotFoundException e) {

            return UserRole.ROLE_USER;
        }
    }
}
