package pl.marwit44.orderNowportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;
import pl.marwit44.orderNowportal.WebUtils;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.exceptions.UserNotFoundException;
import pl.marwit44.orderNowportal.service.AuthorizationService;
import pl.marwit44.orderNowportal.service.CookieModelService;
import pl.marwit44.orderNowportal.service.UserService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Controller
public class LoginController {

    private final UserService userService;
    private final AuthorizationService authorizationService;
    private final WebUtils webUtils;
    private final CookieModelService cookieModelService;

    @Autowired
    public LoginController(UserService userService, AuthorizationService authorizationService, WebUtils webUtils, CookieModelService cookieModelService) {
        this.userService = userService;
        this.authorizationService = authorizationService;
        this.webUtils = webUtils;
        this.cookieModelService = cookieModelService;
    }

    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/generateToken")
    public String generateToken(Model model, HttpServletResponse response) throws UnsupportedEncodingException {

        try {
            User user = userService.getLoggedUser();
            user = userService.setLoginParamsAndGetBackUser(user, webUtils.getClientIp());

            String token = authorizationService.generateToken(user.getId(), user.getLastLogin(), user.getRole());

            Cookie cookie = new Cookie("authorizationToken", URLEncoder.encode(token, "UTF-8"));
            cookie.setPath("/");
            response.addCookie(cookie);

        } catch (UserNotFoundException e) {
            model.addAttribute("exception", e.getMessage());
        }

        return "redirect:/";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/deleteToken")
    public String logout(RedirectAttributesModelMap model, HttpServletResponse response) throws UnsupportedEncodingException {

        cookieModelService.setOrGetCookieValue("", "", "authorizationToken", response);
        cookieModelService.setOrGetCookieValue("", "", "searchTermCookie", response);
        cookieModelService.setOrGetCookieValue("" + 1, "" + 1, "pageIdCookie", response);
        cookieModelService.setOrGetCookieValue("name", "name", "sortByCookie", response);
        cookieModelService.setOrGetCookieValue(String.valueOf(false), String.valueOf(false), "descCookie", response);

        model.addFlashAttribute("logout", true);

        return "redirect:/login";
    }

    @GetMapping("/loginError")
    public String loginError(RedirectAttributesModelMap model) {

        model.addFlashAttribute("error", true);

        return "redirect:/login";
    }

    @GetMapping("/accessError")
    public String accessError() {
        return "redirect:/error403";
    }

    @GetMapping("/error403")
    public String error403() {
        return "error403";
    }

    @PostMapping("/accessError")
    public String accessErrorPost() {
        return "redirect:/error403";
    }

    @PostMapping("/error403")
    public String error403Post() {
        return "error403";
    }
}
