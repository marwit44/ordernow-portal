package pl.marwit44.orderNowportal.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.marwit44.orderNowportal.ValidationJsonResponse;
import pl.marwit44.orderNowportal.dto.CookieValues;
import pl.marwit44.orderNowportal.dto.ItemInOrder;
import pl.marwit44.orderNowportal.dto.Order;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.exceptions.ItemNotFoundException;
import pl.marwit44.orderNowportal.exceptions.OrderNotFoundException;
import pl.marwit44.orderNowportal.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowportal.exceptions.UserNotFoundException;
import pl.marwit44.orderNowportal.model.OrderStatus;
import pl.marwit44.orderNowportal.model.UserRole;
import pl.marwit44.orderNowportal.service.AuthorizationService;
import pl.marwit44.orderNowportal.service.CookieModelService;
import pl.marwit44.orderNowportal.service.ItemInOrderService;
import pl.marwit44.orderNowportal.service.ItemService;
import pl.marwit44.orderNowportal.service.OrderService;
import pl.marwit44.orderNowportal.service.UserService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping("/itemInOrder")
public class ItemInOrderController {

    private final OrderService orderService;
    private final ItemInOrderService itemInOrderService;
    private final ItemService itemService;
    private final UserService userService;
    private final AuthorizationService authorizationService;
    private final CookieModelService cookieModelService;

    private final Logger log = LoggerFactory.getLogger(ItemInOrderController.class);

    @Autowired
    public ItemInOrderController(OrderService orderService, ItemInOrderService itemInOrderService, ItemService itemService, UserService userService, AuthorizationService authorizationService, CookieModelService cookieModelService) {
        this.orderService = orderService;
        this.itemInOrderService = itemInOrderService;
        this.itemService = itemService;
        this.userService = userService;
        this.authorizationService = authorizationService;
        this.cookieModelService = cookieModelService;
    }

    @GetMapping("/shoppingCard")
    public String getShoppingCard(Model model, @RequestParam(required = false) Integer pageId, @RequestParam(required = false) String sortBy, @RequestParam(required = false) String searchTerm, @RequestParam(required = false) String desc,
                                  @RequestParam(required = false) String rangeFrom, @RequestParam(required = false) String rangeTo, @CookieValue(name = "rangeFromCookie", defaultValue = "") String rangeFromCookieVal, @CookieValue(name = "rangeToCookie", defaultValue = "") String rangeToCookieVal,
                                  @CookieValue(name = "searchTermCookie", defaultValue = "") String searchTermCookieVal, @CookieValue(name = "pageIdCookie", defaultValue = "" + 1) String pageIdCookieVal, @CookieValue(name = "descCookie", defaultValue = "false") String descCookieVal,
                                  @CookieValue(name = "sortByCookie", defaultValue = "name") String sortByCookieVal, HttpServletResponse response) throws UnsupportedEncodingException {

        CookieValues cookieValues = cookieModelService.setBasicCookieValues(searchTerm, searchTermCookieVal, "searchTermCookie", "" + pageId, pageIdCookieVal, "pageIdCookie", sortBy, sortByCookieVal, "sortByCookie",
                desc, descCookieVal, "descCookie", rangeFrom, rangeFromCookieVal, "rangeFromCookie", rangeTo, rangeToCookieVal, "rangeToCookie", response);

        User user;

        try {
            user = userService.getLoggedUser();
        } catch (UserNotFoundException e) {
            model.addAttribute("exception", e.getMessage());

            return "shopping_card_is_empty";
        }

        Order order = orderService.getNotAccepted(user.getId());

        try {

            if (order != null) {
                model.addAttribute("orderPrice", order.getPrice());

                itemInOrderService.getListItemInOrderByIdWithPaginationAndFiltering(model, order.getId(), cookieValues.getPageId(), cookieValues.getSearchTerm(), cookieValues.getSortBy(), cookieValues.isDesc(),
                        Integer.parseInt(cookieValues.getRangeFrom()), Integer.parseInt(cookieValues.getRangeTo()), cookieValues.isChangeFrom(), cookieValues.isChangeTo());

                return "shopping_card";
            } else {
                return "shopping_card_is_empty";
            }

        } catch (NumberFormatException e) {

            log.error(e.getMessage());

            itemInOrderService.getListItemInOrderByIdWithPaginationAndFiltering(model, order.getId(), cookieValues.getPageId(), cookieValues.getSearchTerm(), cookieValues.getSortBy(), cookieValues.isDesc(),
                    -1, -1, cookieValues.isChangeFrom(), cookieValues.isChangeTo());

            cookieModelService.setBasicExceptionState(model, cookieValues, e);

            return "shopping_card";
        }
    }

    @GetMapping("/details/{orderId}")
    public String getOrderDetails(Model model, @PathVariable Long orderId, @RequestParam(required = false) Integer pageId, @RequestParam(required = false) String sortBy, @RequestParam(required = false) String searchTerm, @RequestParam(required = false) String desc,
                                  @RequestParam(required = false) String rangeFrom, @RequestParam(required = false) String rangeTo, @CookieValue(name = "rangeFromCookie", defaultValue = "") String rangeFromCookieVal, @CookieValue(name = "rangeToCookie", defaultValue = "") String rangeToCookieVal,
                                  @CookieValue(name = "searchTermCookie", defaultValue = "") String searchTermCookieVal, @CookieValue(name = "pageIdCookie", defaultValue = "" + 1) String pageIdCookieVal, @CookieValue(name = "descCookie", defaultValue = "false") String descCookieVal,
                                  @CookieValue(name = "sortByCookie", defaultValue = "name") String sortByCookieVal, @CookieValue(name = "authorizationToken", defaultValue = "") String token, HttpServletResponse response) throws UnsupportedEncodingException {
        try {

            Order order = orderService.getOrderById(orderId);
            User loggedUser = userService.getLoggedUser();

            User user = authorizationService.validateToken(URLDecoder.decode(token, "UTF-8"), loggedUser.getRole());

            if (order.getUserId().equals(user.getId()) || (loggedUser.getRole() == UserRole.ROLE_ADMIN && loggedUser.getId().equals(user.getId()))) {

                CookieValues cookieValues = cookieModelService.setBasicCookieValues(searchTerm, searchTermCookieVal, "searchTermCookie", "" + pageId, pageIdCookieVal, "pageIdCookie", sortBy, sortByCookieVal, "sortByCookie",
                        desc, descCookieVal, "descCookie", rangeFrom, rangeFromCookieVal, "rangeFromCookie", rangeTo, rangeToCookieVal, "rangeToCookie", response);

                try {
                    model.addAttribute("orderPrice", order.getPrice());
                    model.addAttribute("orderId", orderId);
                    model.addAttribute("orderStatus", order.getStatus());

                    itemInOrderService.getListItemInOrderByIdWithPaginationAndFiltering(model, orderId, cookieValues.getPageId(), cookieValues.getSearchTerm(), cookieValues.getSortBy(), cookieValues.isDesc(),
                            Integer.parseInt(cookieValues.getRangeFrom()), Integer.parseInt(cookieValues.getRangeTo()), cookieValues.isChangeFrom(), cookieValues.isChangeTo());

                    return "orders_details";

                } catch (NumberFormatException e) {
                    log.error(e.getMessage());

                    itemInOrderService.getListItemInOrderByIdWithPaginationAndFiltering(model, order.getId(), cookieValues.getPageId(), cookieValues.getSearchTerm(), cookieValues.getSortBy(), cookieValues.isDesc(),
                            -1, -1, cookieValues.isChangeFrom(), cookieValues.isChangeTo());

                    cookieModelService.setBasicExceptionState(model, cookieValues, e);

                    return "orders_details";
                }

            } else {
                return "error403";
            }

        } catch (OrderNotFoundException | UserNotFoundException e) {
            log.error(e.getMessage());
            model.addAttribute("exception", e.getMessage());

            return "orders_is_empty";
        }
    }

    @GetMapping("/delete")
    public String getDeleteOrder(@RequestParam(name = "id") Long id, Model model, @CookieValue(name = "authorizationToken", defaultValue = "") String token) throws UnsupportedEncodingException {

        try {
            Order order = orderService.getOrderById(id);
            User loggedUser = userService.getLoggedUser();

            User user = authorizationService.validateToken(URLDecoder.decode(token, "UTF-8"), loggedUser.getRole());

            if (order.getUserId().equals(user.getId()) && order.getStatus() == OrderStatus.PREPARATION) {

                order.setOrderType("shoppingCard");

                model.addAttribute("orderObject", order);
                model.addAttribute("disableSave", false);

                return "delete_order_modal";
            } else {
                return "error403";
            }
        } catch (OrderNotFoundException | UserNotFoundException e) {
            log.error(e.getMessage());

            Order order = new Order();
            order.setOrderType("shoppingCard");

            model.addAttribute("orderObject", order);
            model.addAttribute("exception", e.getMessage());
            model.addAttribute("disableSave", true);

            return "delete_order_modal";
        }
    }

    @GetMapping("/deleteItem")
    public String getDeleteItemFromOrder(@RequestParam(name = "id") Long id, Model model, @CookieValue(name = "authorizationToken", defaultValue = "") String token) throws UnsupportedEncodingException {

        try {

            ItemInOrder itemInOrder = itemInOrderService.getItemById(id);

            Order order = orderService.getOrderById(itemInOrder.getOrder().getId());
            User loggedUser = userService.getLoggedUser();

            User user = authorizationService.validateToken(URLDecoder.decode(token, "UTF-8"), loggedUser.getRole());

            if (order.getUserId().equals(user.getId())) {

                model.addAttribute("itemObject", itemInOrder);
                model.addAttribute("orderObject", itemInOrder.getOrder());
                model.addAttribute("disableSave", false);

                return "delete_item_from_order_modal";
            } else {
                return "error403";
            }
        } catch (ItemNotFoundException | OrderNotFoundException | UserNotFoundException e) {
            log.error(e.getMessage());

            ItemInOrder itemInOrder = new ItemInOrder();

            Order order = new Order(1L);
            model.addAttribute("orderObject", order);
            model.addAttribute("itemObject", itemInOrder);
            model.addAttribute("exception", e.getMessage());
            model.addAttribute("disableSave", true);

            return "delete_item_from_order_modal";
        }
    }

    @RequestMapping(value = "/deleteItem", method = {RequestMethod.POST}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ValidationJsonResponse deleteItemFromOrder(@ModelAttribute("itemObject") @Valid ItemInOrder itemInOrder, BindingResult bindingResult, @ModelAttribute("orderId") @Valid Long orderId, @CookieValue(name = "authorizationToken", defaultValue = "") String token) throws UnsupportedEncodingException, OrderNotFoundException {

        ValidationJsonResponse response = new ValidationJsonResponse();

        try {

            Order order = orderService.getOrderById(orderId);
            User loggedUser = userService.getLoggedUser();

            User user = authorizationService.validateToken(URLDecoder.decode(token, "UTF-8"), loggedUser.getRole());

            if (order.getUserId().equals(user.getId())) {
                ItemInOrder oldItemInOrder = itemInOrderService.getItemById(itemInOrder.getId());

                if (!bindingResult.hasErrors() && itemInOrder.getAmount() <= Objects.requireNonNull(oldItemInOrder).getAmount() && itemInOrder.getAmount() > 0) {

                    itemInOrderService.deleteItemFromOrder(itemInOrder, orderId);
                    response.setValidated(true);
                } else {

                    Map<String, List<String>> errorsMap = itemService.getErrorsForItem(bindingResult, false);

                    if (itemInOrder.getAmount() > Objects.requireNonNull(oldItemInOrder).getAmount()) {
                        List<String> errors = new ArrayList<>();
                        errors.add("amount more than maximum");
                        errorsMap.put("amount", errors);
                    }

                    if (itemInOrder.getAmount() < 1 && errorsMap.size() == 0) {
                        List<String> errors = new ArrayList<>();
                        errors.add("amount less than 1");
                        errorsMap.put("amount", errors);
                    }

                    response.setValidated(false);
                    response.setErrorMessages(errorsMap);
                }

            } else {

                response.setAccessDenied(true);

            }
            return response;
        } catch (UserNotFoundException | TokenAuthorizationException | ItemNotFoundException e) {
            log.error(e.getMessage());

            Map<String, List<String>> errorsMap = itemService.getErrorExceptionMessage(e.getMessage());

            response.setErrorMessages(errorsMap);
            response.setValidated(false);

            return response;
        }
    }
}
