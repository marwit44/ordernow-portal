package pl.marwit44.orderNowportal.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;
import pl.marwit44.orderNowportal.ValidationJsonResponse;
import pl.marwit44.orderNowportal.dto.CookieValues;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.exceptions.ItemAmountNotEnoughException;
import pl.marwit44.orderNowportal.exceptions.ItemNotFoundException;
import pl.marwit44.orderNowportal.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowportal.exceptions.UserNotFoundException;
import pl.marwit44.orderNowportal.form.ItemChangeForm;
import pl.marwit44.orderNowportal.form.ItemForm;
import pl.marwit44.orderNowportal.service.CookieModelService;
import pl.marwit44.orderNowportal.service.ItemService;
import pl.marwit44.orderNowportal.service.UserService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/items")
public class ItemController {

    private final ItemService itemService;
    private final UserService userService;
    private final CookieModelService cookieModelService;

    private final Logger log = LoggerFactory.getLogger(ItemController.class);

    @Autowired
    public ItemController(ItemService itemService, UserService userService, CookieModelService cookieModelService) {
        this.itemService = itemService;
        this.userService = userService;
        this.cookieModelService = cookieModelService;
    }

    @GetMapping
    public String getAll(Model model, @RequestParam(required = false) Integer pageId, @RequestParam(required = false) String searchTerm, @RequestParam(required = false) String sortBy, @RequestParam(required = false) String desc,
                         @RequestParam(required = false) String rangeFrom, @RequestParam(required = false) String rangeTo, @CookieValue(name = "rangeFromCookie", defaultValue = "") String rangeFromCookieVal, @CookieValue(name = "rangeToCookie", defaultValue = "") String rangeToCookieVal,
                         @CookieValue(name = "searchTermCookie", defaultValue = "") String searchTermCookieVal, @CookieValue(name = "pageIdCookie", defaultValue = "" + 1) String pageIdCookieVal, @CookieValue(name = "descCookie", defaultValue = "false") String descCookieVal,
                         @CookieValue(name = "sortByCookie", defaultValue = "name") String sortByCookieVal, HttpServletResponse response) throws UnsupportedEncodingException {

        CookieValues cookieValues = cookieModelService.setBasicCookieValues(searchTerm, searchTermCookieVal, "searchTermCookie", "" + pageId, pageIdCookieVal, "pageIdCookie", sortBy, sortByCookieVal, "sortByCookie",
                desc, descCookieVal, "descCookie", rangeFrom, rangeFromCookieVal, "rangeFromCookie", rangeTo, rangeToCookieVal, "rangeToCookie", response);

        try {
            User user = userService.getLoggedUser();
            model.addAttribute("user", user);

            itemService.getListItemWithPaginationAndFiltering(model, cookieValues.getPageId(), cookieValues.getSearchTerm(), cookieValues.getSortBy(), cookieValues.isDesc(),
                    Integer.parseInt(cookieValues.getRangeFrom()), Integer.parseInt(cookieValues.getRangeTo()), cookieValues.isChangeFrom(), cookieValues.isChangeTo());

            return "items";

        } catch (UserNotFoundException | NumberFormatException e) {

            itemService.getListItemWithPaginationAndFiltering(model, cookieValues.getPageId(), cookieValues.getSearchTerm(), cookieValues.getSortBy(), cookieValues.isDesc(),
                    -1, -1, cookieValues.isChangeFrom(), cookieValues.isChangeTo());

            cookieModelService.setBasicExceptionState(model, cookieValues, e);

            return "items";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/add")
    public String addItem(Model model) {

        ItemForm itemForm = new ItemForm();

        model.addAttribute("itemFormObject", itemForm);

        return "add_item_modal";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/edit")
    public String editItem(@RequestParam(name = "id") Long id, Model model) {

        try {
            ItemForm itemForm = itemService.getItemById(id);
            ItemChangeForm itemChangeForm = new ItemChangeForm(itemForm);

            model.addAttribute("itemFormObject", itemChangeForm);
            model.addAttribute("disableSave", false);

            return "edit_item_modal";
        } catch (ItemNotFoundException e) {

            ItemChangeForm itemChangeForm = new ItemChangeForm(null);

            model.addAttribute("itemFormObject", itemChangeForm);
            model.addAttribute("exception", e.getMessage());
            model.addAttribute("disableSave", true);

            return "edit_item_modal";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/save", method = {RequestMethod.POST}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ValidationJsonResponse saveItem(@ModelAttribute @Valid ItemForm itemForm, BindingResult bindingResult) {

        ValidationJsonResponse response = new ValidationJsonResponse();

        if (!bindingResult.hasErrors()) {
            try {
                itemService.saveItem(itemForm);

                response.setValidated(true);
                response.setItemForm(itemForm);
            } catch (Exception | TokenAuthorizationException e) {
                response.setValidated(false);

                Map<String, List<String>> errorsMap = itemService.getErrorExceptionMessage(e.getMessage());

                response.setErrorMessages(errorsMap);
            }
        } else {
            Map<String, List<String>> errorsMap = itemService.getErrorsForItem(bindingResult, false);

            response.setValidated(false);
            response.setErrorMessages(errorsMap);
        }
        return response;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/saveEditItem", method = {RequestMethod.POST}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ValidationJsonResponse saveEditItem(@ModelAttribute @Valid ItemChangeForm itemChangeForm, BindingResult bindingResult) {

        ValidationJsonResponse response = new ValidationJsonResponse();
        boolean sameName = false;

        try {
            sameName = itemService.checkDifferentNames(itemChangeForm);
        } catch (ItemNotFoundException e) {
            log.info(e.getMessage());
        }

        if (!bindingResult.hasErrors() && !sameName) {
            try {

                ItemForm itemForm = new ItemForm(itemChangeForm);

                itemService.saveItem(itemForm);

                response.setValidated(true);
                response.setItemForm(itemForm);
            } catch (Exception | TokenAuthorizationException e) {
                response.setValidated(false);

                Map<String, List<String>> errorsMap = itemService.getErrorExceptionMessage(e.getMessage());

                response.setErrorMessages(errorsMap);
            }
        } else {
            Map<String, List<String>> errorsMap = itemService.getErrorsForItem(bindingResult, sameName);

            response.setValidated(false);
            response.setErrorMessages(errorsMap);
        }
        return response;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/delete")
    public String getDeleteItem(@RequestParam(name = "id") Long id, Model model) {

        try {

            ItemForm itemForm = itemService.getItemById(id);
            ItemChangeForm itemChangeForm = new ItemChangeForm(itemForm);

            model.addAttribute("itemFormObject", itemChangeForm);
            model.addAttribute("disableSave", false);

            return "delete_item_modal";
        } catch (ItemNotFoundException e) {

            ItemChangeForm itemChangeForm = new ItemChangeForm(null);

            model.addAttribute("itemFormObject", itemChangeForm);
            model.addAttribute("exception", e.getMessage());
            model.addAttribute("disableSave", true);

            return "delete_item_modal";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/delete")
    public String deleteItem(@ModelAttribute @Valid ItemChangeForm itemChangeForm, RedirectAttributesModelMap modelRedirect) {

        ItemForm itemForm = new ItemForm(itemChangeForm);

        try {

            if (itemService.checkDeletePossible(itemForm.getName())) {
                itemService.deleteItem(itemForm);
            } else {
                modelRedirect.addFlashAttribute("exception", "can't delete item " + itemForm.getName()
                        + ", minimum one order in pending");
            }
        } catch (TokenAuthorizationException e) {

            log.error(e.getMessage(), e);
            modelRedirect.addFlashAttribute("exception", e.getMessage());
        }

        return "redirect:/items";
    }

    @GetMapping("/buy")
    public String getBuyItem(@RequestParam(name = "id") Long id, Model model) {

        try {
            ItemForm itemForm = itemService.getItemById(id);

            ItemChangeForm itemChangeForm = new ItemChangeForm(itemForm);
            itemChangeForm.setAmount(1);

            model.addAttribute("itemFormObject", itemChangeForm);
            model.addAttribute("disableSave", false);

            return "buy_item_modal";
        } catch (ItemNotFoundException e) {

            ItemChangeForm itemChangeForm = new ItemChangeForm(null);
            itemChangeForm.setAmount(1);

            model.addAttribute("itemFormObject", itemChangeForm);
            model.addAttribute("exception", e.getMessage());
            model.addAttribute("disableSave", true);

            return "buy_item_modal";
        }
    }

    @RequestMapping(value = "/buy", method = {RequestMethod.POST}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ValidationJsonResponse buyItem(@ModelAttribute @Valid ItemChangeForm itemChangeForm, BindingResult bindingResult) throws ItemNotFoundException {

        ValidationJsonResponse response = new ValidationJsonResponse();

        if (!bindingResult.hasErrors()) {

            try {

                ItemForm itemForm = new ItemForm(itemChangeForm);

                User user = userService.getLoggedUser();

                itemService.buyItem(itemForm, user.getId());

                response.setValidated(true);
                response.setItemForm(itemForm);
            } catch (ItemAmountNotEnoughException | UserNotFoundException | TokenAuthorizationException e) {
                log.error(e.getMessage(), e);

                response.setValidated(false);

                Map<String, List<String>> errorsMap = itemService.getErrorExceptionMessage(e.getMessage());

                response.setErrorMessages(errorsMap);
            }
        } else {

            Map<String, List<String>> errorsMap = itemService.getErrorsForItem(bindingResult, false);

            response.setValidated(false);
            response.setErrorMessages(errorsMap);
        }
        return response;
    }
}
