package pl.marwit44.orderNowportal.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;
import pl.marwit44.orderNowportal.dto.CookieValues;
import pl.marwit44.orderNowportal.dto.Image;
import pl.marwit44.orderNowportal.dto.ItemDetails;
import pl.marwit44.orderNowportal.exceptions.ItemDetailsNotFoundException;
import pl.marwit44.orderNowportal.exceptions.ItemNotFoundException;
import pl.marwit44.orderNowportal.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowportal.exceptions.UserNotFoundException;
import pl.marwit44.orderNowportal.form.ItemForm;
import pl.marwit44.orderNowportal.service.CookieModelService;
import pl.marwit44.orderNowportal.service.ItemDetailsService;
import pl.marwit44.orderNowportal.service.ItemService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.List;

@Controller
@RequestMapping("/itemsDetails")
public class ItemDetailsController {

    private final ItemService itemService;
    private final ItemDetailsService itemDetailsService;
    private final CookieModelService cookieModelService;

    private final Logger log = LoggerFactory.getLogger(ItemDetailsController.class);

    @Autowired
    public ItemDetailsController(ItemService itemService, ItemDetailsService itemDetailsService, CookieModelService cookieModelService) {
        this.itemService = itemService;
        this.itemDetailsService = itemDetailsService;
        this.cookieModelService = cookieModelService;
    }

    @GetMapping
    public String getById(Model model, @ModelAttribute ItemDetails itemDetails, @RequestParam() Long id, @RequestParam(required = false) Integer pictureNumber,
                          @RequestParam(required = false) String goBackTo, @CookieValue(name = "goBackToCookie", defaultValue = "items") String goBackToCookieVal,
                          @CookieValue(name = "pictureNumberCookie", defaultValue = "" + 0) String pictureNumberCookieVal,
                          @CookieValue(name = "orderIdCookie", defaultValue = "" + 0) String orderIdCookieVal, HttpServletResponse response) {

        try {
            CookieValues cookieValues = cookieModelService.setItemDetailsCookies("" + pictureNumber, pictureNumberCookieVal, "pictureNumberCookie", goBackTo, goBackToCookieVal, "goBackToCookie", orderIdCookieVal, orderIdCookieVal, "orderIdCookie", response);

            ItemForm item = itemService.getItemById(id);
            itemDetailsService.getItemDetails(model, item, id, null, cookieValues.getGoBackTo(), cookieValues.getOrderId(), cookieValues.getPictureNumber());
        } catch (ItemNotFoundException | ItemDetailsNotFoundException | UserNotFoundException | UnsupportedEncodingException e) {
            log.error(e.getMessage());
            model.addAttribute("errorMessage", e.getMessage());
        }

        return "itemDetails";
    }

    @GetMapping("/getByName/{goBackTo}")
    public String getByName(Model model, @RequestParam() String name, @PathVariable() String goBackTo, @RequestParam(required = false) Long orderId, @CookieValue(name = "goBackToCookie", defaultValue = "items") String goBackToCookieVal,
                            @RequestParam(required = false) Integer pictureNumber, @CookieValue(name = "pictureNumberCookie", defaultValue = "" + 0) String pictureNumberCookieVal,
                            @CookieValue(name = "orderIdCookie", defaultValue = "" + 0) String orderIdCookieVal, HttpServletResponse response) {

        try {
            CookieValues cookieValues = cookieModelService.setItemDetailsCookies("" + pictureNumber, pictureNumberCookieVal, "pictureNumberCookie", goBackTo, goBackToCookieVal, "goBackToCookie", "" + orderId, orderIdCookieVal, "orderIdCookie", response);

            ItemForm item = itemService.getItemByName(name);
            itemDetailsService.getItemDetails(model, item, item.getId(), null, cookieValues.getGoBackTo(), cookieValues.getOrderId(), cookieValues.getPictureNumber());
        } catch (ItemNotFoundException | ItemDetailsNotFoundException | UserNotFoundException | UnsupportedEncodingException e) {
            log.error(e.getMessage());
            model.addAttribute("errorMessage", e.getMessage());
        }

        return "itemDetails";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/save")
    public String saveDetails(@ModelAttribute @Valid ItemDetails itemDetails, BindingResult bindingResult, Model model, RedirectAttributesModelMap modelRedirect,
                              @CookieValue(name = "pictureNumberCookie", defaultValue = "" + 1) String pictureNumberCookieVal) {

        if (!bindingResult.hasErrors()) {

            try {
                itemDetailsService.saveDetails(itemDetails);
            } catch (TokenAuthorizationException e) {
                log.info(e.getMessage());
            }

            modelRedirect.addFlashAttribute("successText", "Item details save success");
            modelRedirect.addFlashAttribute("itemDetails", itemDetails);

            return "redirect:/itemsDetails?id=" + itemDetails.getId();
        } else {
            try {

                ItemForm item = itemService.getItemById(itemDetails.getId());
                itemDetailsService.getItemDetails(model, item, item.getId(), itemDetails, itemDetails.getGoBackTo(), itemDetails.getOrderId(), Integer.parseInt(pictureNumberCookieVal));

            } catch (ItemNotFoundException | UserNotFoundException | ItemDetailsNotFoundException e) {
                log.info(e.getMessage());
                model.addAttribute("errorMessage", e.getMessage());
            }

            return "itemDetails";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/addImage")
    public String addImage(@RequestParam() Long id, Model model) {

        model.addAttribute("detailsId", id);

        return "add_image_modal";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/saveImage")
    public String saveImage(@ModelAttribute MultipartFile file, @ModelAttribute("detailsId") @Valid Long detailsId, RedirectAttributesModelMap modelRedirect) {

        try {

            ItemDetails itemDetails = itemDetailsService.getItemDetailsById(detailsId);

            Image image = new Image();

            image.setName(file.getOriginalFilename());
            image.setType(file.getContentType());
            image.setPicture(itemDetailsService.compressBytes(file.getBytes()));
            image.setItemDetails(itemDetails);

            if (!image.getName().equals("")) {
                itemDetailsService.saveImage(image);
            } else {
                modelRedirect.addFlashAttribute("exception", "No image added");
            }

        } catch (Exception | ItemDetailsNotFoundException | TokenAuthorizationException e) {

            log.error(e.getMessage(), e);
            modelRedirect.addFlashAttribute("exception", e.getMessage());
        }

        return "redirect:/itemsDetails?id=" + detailsId;
    }

    @RequestMapping(value = "getImage")
    @ResponseBody
    public byte[] getImage(@RequestParam() Long detailsId, RedirectAttributesModelMap modelRedirect, @CookieValue(name = "pictureNumberCookie", defaultValue = "" + 0) String pictureNumberStr) {

        try {
            ItemDetails itemDetails = itemDetailsService.getItemDetailsById(detailsId);
            if (itemDetails.getImageList() != null) {

                int pictureNumber = Integer.parseInt(pictureNumberStr);
                int imageListSize = itemDetails.getImageList().size();

                if (pictureNumber < 1) {
                    pictureNumber = 1;
                }

                if (pictureNumber > imageListSize) {
                    pictureNumber = imageListSize;
                }

                Image image = itemDetails.getImageList().get(pictureNumber - 1);

                return itemDetailsService.decompressBytes(image.getPicture());
            }
        } catch (ItemDetailsNotFoundException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/deleteImage")
    public String deleteImage(@RequestParam() Long id, @RequestParam() int pictureNumber, Model model) {

        model.addAttribute("detailsId", id);
        model.addAttribute("pictureNumber", pictureNumber);

        return "delete_image_modal";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/deleteImage")
    public String deleteImage(@ModelAttribute("detailsId") @Valid Long detailsId, @ModelAttribute("pictureNumber") @Valid int pictureNumber, RedirectAttributesModelMap modelRedirect) {

        try {

            List<Image> imageList = itemDetailsService.getItemDetailsById(detailsId).getImageList();

            if (imageList.size() > 0 && imageList.size() >= pictureNumber) {

                Image image = imageList.get(pictureNumber - 1);
                itemDetailsService.deleteImage(image);
            } else {
                modelRedirect.addFlashAttribute("exception", "No image to delete");
            }

        } catch (Exception | ItemDetailsNotFoundException | TokenAuthorizationException e) {

            log.error(e.getMessage(), e);
            modelRedirect.addFlashAttribute("exception", e.getMessage());
        }

        return "redirect:/itemsDetails?id=" + detailsId;
    }
}
