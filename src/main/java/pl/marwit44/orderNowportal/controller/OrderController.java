package pl.marwit44.orderNowportal.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;
import pl.marwit44.orderNowportal.dto.CookieValues;
import pl.marwit44.orderNowportal.dto.Order;
import pl.marwit44.orderNowportal.dto.User;
import pl.marwit44.orderNowportal.exceptions.OrderNotFoundException;
import pl.marwit44.orderNowportal.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowportal.exceptions.UserNotFoundException;
import pl.marwit44.orderNowportal.model.OrderStatus;
import pl.marwit44.orderNowportal.model.UserRole;
import pl.marwit44.orderNowportal.service.AuthorizationService;
import pl.marwit44.orderNowportal.service.CookieModelService;
import pl.marwit44.orderNowportal.service.OrderService;
import pl.marwit44.orderNowportal.service.UserService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.time.DateTimeException;
import java.util.List;

@Controller
@RequestMapping("/orders")
public class OrderController {

    private final OrderService orderService;
    private final UserService userService;
    private final AuthorizationService authorizationService;
    private final CookieModelService cookieModelService;

    private final Logger log = LoggerFactory.getLogger(ItemController.class);

    @Autowired
    public OrderController(OrderService orderService, UserService userService, AuthorizationService authorizationService, CookieModelService cookieModelService) {
        this.orderService = orderService;
        this.userService = userService;
        this.authorizationService = authorizationService;
        this.cookieModelService = cookieModelService;
    }

    @GetMapping
    public String getAll(Model model, @RequestParam(required = false) Integer pageId, @RequestParam(required = false) String searchTerm, @RequestParam(required = false) String sortBy, @RequestParam(required = false) String desc,
                         @RequestParam(required = false) String rangeFrom, @RequestParam(required = false) String rangeTo, @CookieValue(name = "orderRangeFromCookie", defaultValue = "") String rangeFromCookieVal, @CookieValue(name = "orderRangeToCookie", defaultValue = "") String rangeToCookieVal,
                         @CookieValue(name = "orderSearchTermCookie", defaultValue = "") String searchTermCookieVal, @CookieValue(name = "orderPageIdCookie", defaultValue = "" + 1) String pageIdCookieVal, @CookieValue(name = "orderDescCookie", defaultValue = "false") String descCookieVal,
                         @CookieValue(name = "orderSortByCookie", defaultValue = "status") String sortByCookieVal, HttpServletResponse response) throws UnsupportedEncodingException {

        CookieValues cookieValues = cookieModelService.setBasicCookieValues(searchTerm, searchTermCookieVal, "orderSearchTermCookie", "" + pageId, pageIdCookieVal, "orderPageIdCookie", sortBy, sortByCookieVal, "orderSortByCookie",
                desc, descCookieVal, "orderDescCookie", rangeFrom, rangeFromCookieVal, "orderRangeFromCookie", rangeTo, rangeToCookieVal, "orderRangeToCookie", response);

        User user;

        try {
            user = userService.getLoggedUser();
        } catch (UserNotFoundException e) {
            model.addAttribute("exception", e.getMessage());

            return "orders_is_empty";
        }

        List<Order> allOrders;

        try {

            allOrders = orderService.getListOrdersWithPaginationAndFiltering(model, user, cookieValues.getPageId(), cookieValues.getSearchTerm(), cookieValues.getSortBy(), cookieValues.isDesc(),
                    cookieValues.getRangeFrom(), cookieValues.getRangeTo(), cookieValues.isChangeFrom(), cookieValues.isChangeTo());

        } catch (DateTimeException | NumberFormatException e) {

            allOrders = orderService.getListOrdersWithPaginationAndFiltering(model, user, cookieValues.getPageId(), cookieValues.getSearchTerm(), cookieValues.getSortBy(), cookieValues.isDesc(),
                    "-1", "-1", cookieValues.isChangeFrom(), cookieValues.isChangeTo());

            cookieModelService.setBasicExceptionState(model, cookieValues, e);
        }

        if ((allOrders != null && allOrders.size() != 0) || (searchTerm != null && !searchTerm.equals("")) || user.getRole() == UserRole.ROLE_ADMIN
                || (rangeFrom != null && !rangeFrom.equals(""))|| (rangeTo != null && !rangeTo.equals(""))) {

            return "orders";
        } else {

            return "orders_is_empty";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/edit")
    public String getEditOrder(@RequestParam(name = "id") Long id, Model model) {

        try {

            Order order = orderService.getOrderById(id);
            order.setOrderType("order");

            model.addAttribute("orderObject", order);
            model.addAttribute("disableSave", false);

            return "edit_order_modal";
        } catch (OrderNotFoundException e) {

            Order order = new Order();
            order.setOrderType("order");

            model.addAttribute("orderObject", order);
            model.addAttribute("exception", e.getMessage());
            model.addAttribute("disableSave", true);

            return "edit_order_modal";
        }
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/edit")
    public String editOrder(@ModelAttribute @Valid Order orderForm, RedirectAttributesModelMap modelRedirect) {

        try {

            Order order = orderService.getOrderById(orderForm.getId());

            OrderStatus oldStatus = order.getStatus();
            order.setStatus(orderForm.getStatus());
            orderService.editOrder(order, oldStatus);

            return "redirect:/orders";

        } catch (OrderNotFoundException | TokenAuthorizationException e) {

            log.error(e.getMessage(), e);
            modelRedirect.addFlashAttribute("exception", e.getMessage());

            return "redirect:/orders";
        }
    }

    @GetMapping("/delete")
    public String getDeleteOrder(@RequestParam(name = "id") Long id, Model model, @CookieValue(name = "authorizationToken", defaultValue = "") String token) throws UnsupportedEncodingException {

        try {

            Order order = orderService.getOrderById(id);
            User loggedUser = userService.getLoggedUser();

            User user = authorizationService.validateToken(URLDecoder.decode(token, "UTF-8"), loggedUser.getRole());

            if (order.getUserId().equals(user.getId()) && order.getStatus() == OrderStatus.PREPARATION) {

                order.setOrderType("order");

                model.addAttribute("orderObject", order);
                model.addAttribute("disableSave", false);

                return "delete_order_modal";

            } else {
                return "error403";
            }
        } catch (OrderNotFoundException | UserNotFoundException e) {

            Order order = new Order();
            order.setOrderType("order");

            model.addAttribute("orderObject", order);
            model.addAttribute("exception", e.getMessage());
            model.addAttribute("disableSave", true);

            return "delete_order_modal";
        }
    }

    @PostMapping("/delete")
    public String deleteOrder(@ModelAttribute @Valid Order orderForm, RedirectAttributesModelMap modelRedirect, @CookieValue(name = "authorizationToken", defaultValue = "") String token) throws UnsupportedEncodingException {

        try {

            Order order = orderService.getOrderById(orderForm.getId());
            User loggedUser = userService.getLoggedUser();

            User user = authorizationService.validateToken(URLDecoder.decode(token, "UTF-8"), loggedUser.getRole());

            if (order.getUserId().equals(user.getId()) && order.getStatus() == OrderStatus.PREPARATION) {

                orderService.deleteOrder(order);

                if (orderForm.getOrderType().equals("order")) {
                    return "redirect:/orders";
                } else {
                    return "redirect:/items?pageId=1&sortBy=name&searchTerm=";
                }
            } else {
                return "error403";
            }

        } catch (OrderNotFoundException | UserNotFoundException | TokenAuthorizationException e) {

            log.error(e.getMessage(), e);
            modelRedirect.addFlashAttribute("exception", e.getMessage());

            if (orderForm.getOrderType().equals("order")) {
                return "redirect:/orders";
            } else {
                return "redirect:/itemInOrder/shoppingCard";
            }

        }
    }

    @GetMapping("/accept")
    public String getAcceptOrder(@RequestParam(name = "id") Long id, Model model, @CookieValue(name = "authorizationToken", defaultValue = "") String token) throws UnsupportedEncodingException {

        try {

            Order order = orderService.getOrderById(id);
            User loggedUser = userService.getLoggedUser();

            User user = authorizationService.validateToken(URLDecoder.decode(token, "UTF-8"), loggedUser.getRole());

            if (order.getUserId().equals(user.getId())) {

                model.addAttribute("orderObject", order);
                model.addAttribute("disableSave", false);

                return "accept_order_modal";
            } else {
                return "error403";
            }
        } catch (OrderNotFoundException | UserNotFoundException e) {

            Order order = new Order();

            model.addAttribute("orderObject", order);
            model.addAttribute("exception", e.getMessage());
            model.addAttribute("disableSave", true);

            return "accept_order_modal";
        }
    }

    @PostMapping("/accept")
    public String acceptOrder(@ModelAttribute @Valid Order orderForm, RedirectAttributesModelMap modelRedirect, @CookieValue(name = "authorizationToken", defaultValue = "") String token) throws UnsupportedEncodingException {

        try {

            Order order = orderService.getOrderById(orderForm.getId());
            User loggedUser = userService.getLoggedUser();

            User user = authorizationService.validateToken(URLDecoder.decode(token, "UTF-8"), loggedUser.getRole());

            if (order.getUserId().equals(user.getId())) {

                orderService.acceptOrder(order);

                return "redirect:/orders?pageId=1&searchTerm=";
            } else {
                return "error403";
            }
        } catch (OrderNotFoundException | UserNotFoundException | TokenAuthorizationException e) {

            log.error(e.getMessage(), e);
            modelRedirect.addFlashAttribute("exception", e.getMessage());

            return "redirect:/itemInOrder/shoppingCard";
        }
    }
}
