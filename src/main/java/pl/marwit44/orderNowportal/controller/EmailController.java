package pl.marwit44.orderNowportal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.marwit44.orderNowportal.dto.MailDetails;
import pl.marwit44.orderNowportal.exceptions.EditMailTemplateException;
import pl.marwit44.orderNowportal.service.MailTemplatesService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Controller
@RequestMapping("/email_templates")
public class EmailController {

    private final MailTemplatesService mailTemplatesService;

    @Autowired
    public EmailController(MailTemplatesService mailTemplatesService) {
        this.mailTemplatesService = mailTemplatesService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping({"/list/page/{pageId}", "/list/page/"})
    public String getTemplates(Model model, @PathVariable(required = false) Integer pageId, @RequestParam(required = false) String searchTerm, HttpServletResponse response) throws UnsupportedEncodingException {

        Cookie cookie;
        if (searchTerm != null) {
            cookie = new Cookie("searchTermCookie", URLEncoder.encode(searchTerm, "UTF-8"));
        } else {
            cookie = new Cookie("searchTermCookie", URLEncoder.encode("", "UTF-8"));
        }
        cookie.setPath("/");
        response.addCookie(cookie);

        mailTemplatesService.getListItemWithPaginationAndFiltering(model, pageId, searchTerm);

        return "email_templates";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/details/{id}")
    public String getEmailDetails(@PathVariable Long id, Model model, @CookieValue(name = "searchTermCookie", defaultValue = "") String searchTerm) {

        MailDetails mailDetails = mailTemplatesService.getMailTemplateDetail(id);
        mailDetails.setSearchTerm(searchTerm);

        model.addAttribute("template", mailDetails);

        return "email_details_template";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/edit/{id}")
    public String getEditEmailDetails(@PathVariable Long id, Model model, @CookieValue(name = "searchTermCookie", defaultValue = "") String searchTerm) {

        MailDetails mailDetails = mailTemplatesService.getMailTemplateDetail(id);
        mailDetails.setSearchTerm(searchTerm);

        model.addAttribute("template", mailDetails);
        model.addAttribute("exception", "");

        return "email_edit_template";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/edit")
    public String editEmailDetails(@ModelAttribute MailDetails mailDetails, Model model) {
        try {
            mailTemplatesService.editMailTemplate(mailDetails);
            if(mailDetails.getSearchTerm() != null && !mailDetails.getSearchTerm().equals("")) {
                return "redirect:/email_templates/list/page/1?searchTerm="+mailDetails.getSearchTerm();
            } else {
                return "redirect:/email_templates/list/page/1";
            }

        } catch (EditMailTemplateException e) {
            model.addAttribute("template", mailDetails);
            model.addAttribute("exception", e.getMessage());
            return "email_edit_template";
        }
    }
}