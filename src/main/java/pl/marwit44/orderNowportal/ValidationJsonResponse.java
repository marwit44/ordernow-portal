package pl.marwit44.orderNowportal;

import pl.marwit44.orderNowportal.form.ItemForm;

import java.util.List;
import java.util.Map;

public class ValidationJsonResponse {

    private ItemForm itemForm;
    private boolean validated;
    private boolean accessDenied = false;
    private Map<String, List<String>> errorMessages;

    public ItemForm getItemForm() {
        return itemForm;
    }

    public void setItemForm(ItemForm itemForm) {
        this.itemForm = itemForm;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public boolean isAccessDenied() {
        return accessDenied;
    }

    public void setAccessDenied(boolean accessDenied) {
        this.accessDenied = accessDenied;
    }

    public Map<String, List<String>> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(Map<String, List<String>> errorMessages) {
        this.errorMessages = errorMessages;
    }

}
