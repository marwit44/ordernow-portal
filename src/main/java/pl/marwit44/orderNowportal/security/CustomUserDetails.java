package pl.marwit44.orderNowportal.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.marwit44.orderNowportal.model.UserModel;

import java.util.Collection;
import java.util.Collections;

public class CustomUserDetails extends UserModel implements UserDetails {

    private UserModel returnValue;

    public CustomUserDetails() {
    }

    public CustomUserDetails(UserModel userModel) {
        this.returnValue = userModel;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        GrantedAuthority authority = new SimpleGrantedAuthority(returnValue.getRole().toString());
        return Collections.singletonList(authority);
    }

    @Override
    public String getPassword() {
        return returnValue.getPassword();
    }

    @Override
    public String getUsername() {
        return returnValue.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return returnValue.isEnabled();
    }

    @Override
    public boolean isAccountNonLocked() {
        return returnValue.isEnabled();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return returnValue.isEnabled();
    }

    @Override
    public boolean isEnabled() {
        return returnValue.isEnabled();
    }
}
