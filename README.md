# orderNow-portal

First part of project OrderNow with usage of Spring Boot technology.

Admin credentials:
email: admin477@gmail.com
password: Admin321$

Test users credentials:
email: john.long34@gmail.com
email: paul.lee49@gmail.com
email: holly.moore24@gmail.com

password: Password321$

Admin and users added after execute migration scripts in liquibase 